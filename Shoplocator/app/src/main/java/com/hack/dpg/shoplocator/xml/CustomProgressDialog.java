package com.hack.dpg.shoplocator.xml;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.hack.dpg.shoplocator.R;

@SuppressWarnings("ConstantConditions")
public class CustomProgressDialog extends DialogFragment implements CustomDialogView {

    public CustomProgressDialog dialog;

    public static CustomProgressDialog newInstance() {
        return new CustomProgressDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.custom_progress_dialog, container, false);
    }

    @Override
    public void show(Context context, FragmentManager fragmentManager) {
        this.dialog = CustomProgressDialog.newInstance();
        this.dialog.show(fragmentManager, CustomProgressDialog.class.getName());
    }

    @Override
    public void dismissDialog() throws NullPointerException {
        if (this.dialog == null)
            throw new NullPointerException("CustomProgressDialog is null");
        this.dialog.dismiss();
    }
}