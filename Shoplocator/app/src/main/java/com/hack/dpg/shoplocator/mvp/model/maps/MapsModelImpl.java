package com.hack.dpg.shoplocator.mvp.model.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.MapsModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@SuppressWarnings("Convert2streamapi")
public class MapsModelImpl implements MapsModel {

    @Inject
    public ShopDataBaseController dataBaseController;

    private List<Shop> shopList = new ArrayList<>();

    public MapsModelImpl() {
        ShoplocatorApplication.component()
                .plus(new MapsModule())
                .inject(this);

        this.shopList.addAll(this.dataBaseController.findAll());
    }

    @Override
    public List<MarkerOptions> getShopsMarkers() {
        List<MarkerOptions> markers = new ArrayList<>();
        for (Shop shop : this.shopList)
            markers.add(createMarkerByShop(shop));
        return markers;
    }

    @Override
    public LatLng getDisplaysShopLocation(long shopId) {
        Shop shop = this.dataBaseController.findEntityById(shopId);
        return new LatLng(shop.getLatitude(), shop.getLongitude());
    }

    @Override
    public List<Shop> getShops() {
        return this.shopList;
    }

    @Override
    public int getListPositionByShopId(List<Shop> shops, long shopId) {
        for (int i = 0; i < shops.size(); i++)
            if (shops.get(i).getId() == shopId)
                return i;
        return 0;
    }

    @Override
    public String[] getShopNames() {
        List<String> shopsNames = new ArrayList<>();
        for (Shop shop : this.shopList)
            shopsNames.add(shop.getTitle());
        return shopsNames.toArray(new String[shopsNames.size()]);
    }

    @Override
    public long getShopIdByName(String shopName) {
        return this.dataBaseController.findShopByName(shopName).getId();
    }

    private MarkerOptions createMarkerByShop(Shop shop) {
        LatLng latLng = new LatLng(shop.getLatitude(), shop.getLongitude());
        return new MarkerOptions().position(latLng).title(shop.getTitle());
    }
}
