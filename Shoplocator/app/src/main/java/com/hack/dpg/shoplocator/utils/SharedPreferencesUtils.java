package com.hack.dpg.shoplocator.utils;

public interface SharedPreferencesUtils {

    void setIntState(String key, int value);

    int getIntState(String key);

    boolean hasIntState(String key);

    boolean clearAllStates();
}
