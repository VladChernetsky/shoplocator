package com.hack.dpg.shoplocator.mvp.model.shop;

import android.os.AsyncTask;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.pojo.ShopPojo;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopFormPresenterImpl;
import com.hack.dpg.shoplocator.repository.ShopRepository;
import com.hack.dpg.shoplocator.utils.RegExpUtils;

import javax.inject.Inject;

public class ShopFormModelImpl implements ShopFormModel {

    @Inject
    public ShopDataBaseController controller;

    @Inject
    public ShopRepository shopRepository;

    private long shopId;
    private ShopPojo shopEntity;

    public ShopFormModelImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
        this.shopEntity = new ShopPojo();
    }

    @Override
    public Shop findShop() {
        Shop shop = this.controller.findEntityById(this.shopId);
        if (shop == null)
            shop = new Shop();
        return shop;
    }

    @Override
    public boolean isShopExists() {
        return this.shopId != 0;
    }

    @Override
    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    @Override
    public void saveShop(ShopFormPresenterImpl.onSaveShopCallback callback) {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                Shop shop = convertShopPojoToShop(shopEntity);
                return shopRepository.save(shop);
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                if (response != null) {
                    callback.onError(response);
                } else {
                    callback.onSuccess();
                }
            }
        }.execute();
    }

    @Override
    public void updateTitle(String title) {
        this.shopEntity.setTitle(title);
    }

    @Override
    public void updateDescription(String description) {
        this.shopEntity.setDescription(description);
    }

    @Override
    public void updateImageUrl(String imageUrl) {
        this.shopEntity.setImage(imageUrl);
    }

    @Override
    public void updateLongitude(String longitude) {
        this.shopEntity.setLongitude(longitude);
    }

    @Override
    public void updateLatitude(String latitude) {
        this.shopEntity.setLatitude(latitude);
    }

    @Override
    public void updateOwner(String ownerId) {
        this.shopEntity.setUserId(ownerId);
    }

    @Override
    public void isFormEntitiesValid(ShopFormPresenterImpl.onCheckFieldsCallback listener) {
        if (this.shopEntity.getTitle().isEmpty() || this.shopEntity.getDescription().isEmpty()) {
            listener.onEmptyFieldError();
        } else if (!RegExpUtils.isValidUrl(this.shopEntity.getImage())) {
            listener.onImageUrlError();
        } else if (!isNumericValue(this.shopEntity.getUserId())) {
            listener.onOwnerIdError();
        } else if (!RegExpUtils.isLongitude(this.shopEntity.getLongitude()) || !RegExpUtils.isLatitude(this.shopEntity.getLatitude())) {
            listener.onCoordinatesError();
        } else {
            listener.onSuccess();
        }
    }

    private Shop convertShopPojoToShop(ShopPojo shopPojo) {
        Shop newShop = this.controller.findEntityById(this.shopId);
        long remoteId = newShop != null ? newShop.getShopId() : 0;
        return new Shop.Builder()
                .setShopId(remoteId)
                .setTitle(shopPojo.getTitle())
                .setDescription(shopPojo.getDescription())
                .setImage(shopPojo.getImage())
                .setLatitude(Double.valueOf(shopPojo.getLatitude()))
                .setLongitude(Double.valueOf(shopPojo.getLongitude()))
                .setUserId(Long.valueOf(shopPojo.getUserId()))
                .create();
    }

    private boolean isNumericValue(String value) {
        int size = value.length();
        for (int i = 0; i < size; i++) {
            if (!Character.isDigit(value.charAt(i))) {
                return false;
            }
        }
        return size > 0;
    }
}
