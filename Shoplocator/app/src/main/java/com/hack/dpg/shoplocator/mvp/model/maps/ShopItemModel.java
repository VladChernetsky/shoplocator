package com.hack.dpg.shoplocator.mvp.model.maps;

import com.hack.dpg.shoplocator.data.entity.Shop;

public interface ShopItemModel {

    Shop getShopById(long shopId);
}
