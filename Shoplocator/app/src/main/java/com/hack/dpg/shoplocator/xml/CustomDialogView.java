package com.hack.dpg.shoplocator.xml;

import android.content.Context;
import android.support.v4.app.FragmentManager;

public interface CustomDialogView {

    void show(Context context, FragmentManager fragmentManager);

    void dismissDialog();
}
