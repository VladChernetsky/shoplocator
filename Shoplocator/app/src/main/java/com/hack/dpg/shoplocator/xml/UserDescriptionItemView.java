package com.hack.dpg.shoplocator.xml;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.hack.dpg.shoplocator.R;

public class UserDescriptionItemView extends RelativeLayout {

    public UserDescriptionItemView(Context context) {
        super(context);
        addInnerView(context);
    }

    public UserDescriptionItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        addInnerView(context);
    }

    public UserDescriptionItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addInnerView(context);
    }

    private void addInnerView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_detail_item_view, this, false);
        addView(view);
    }
}
