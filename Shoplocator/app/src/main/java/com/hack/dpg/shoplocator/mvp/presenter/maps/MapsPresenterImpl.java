package com.hack.dpg.shoplocator.mvp.presenter.maps;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.MapsModule;
import com.hack.dpg.shoplocator.mvp.model.maps.MapsModel;
import com.hack.dpg.shoplocator.mvp.view.maps.MapsView;

import javax.inject.Inject;

public class MapsPresenterImpl implements MapsPresenter {

    @Inject
    public MapsModel model;

    private MapsView mapsView;

    public MapsPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new MapsModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(MapsView mapsView) {
        this.mapsView = mapsView;
        this.mapsView.initViewPager(this.model.getShops());
        this.mapsView.initSearchView(this.model.getShopNames());
    }

    @Override
    public void onMapReady(long shopId) {
        this.mapsView.displayMarkers(this.model.getShopsMarkers());
        displayShopById(shopId);
    }

    @Override
    public void onViewedShopChanged(int position) {
        Shop shop = this.model.getShops().get(position);
        this.mapsView.saveActiveShop(shop.getId());
        this.mapsView.moveCameraToMarker(this.model.getDisplaysShopLocation(shop.getId()));
    }

    @Override
    public void onShopFounded(String shopName) {
        displayShopById(this.model.getShopIdByName(shopName));
    }

    private void displayShopById(long shopId) {
        if (shopId != 0) {
            this.mapsView.moveCameraToMarker(this.model.getDisplaysShopLocation(shopId));
            this.mapsView.setViewPagerPositionByShop(this.model.getListPositionByShopId(this.model.getShops(), shopId));
        }
    }
}
