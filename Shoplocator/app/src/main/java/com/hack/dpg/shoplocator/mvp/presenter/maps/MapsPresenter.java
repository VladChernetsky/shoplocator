package com.hack.dpg.shoplocator.mvp.presenter.maps;

import com.hack.dpg.shoplocator.mvp.view.maps.MapsView;

public interface MapsPresenter {

    void onViewCreated(MapsView mapsView);

    void onMapReady(long shopId);

    void onViewedShopChanged(int position);

    void onShopFounded(String shopName);
}
