package com.hack.dpg.shoplocator.mvp.view;

public interface FragmentDetailClickListener {

    void clickShopDetail(long shopId);

    void clickUserDetail(long userId);
}
