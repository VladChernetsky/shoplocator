package com.hack.dpg.shoplocator.mvp.view.settings;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.presenter.settings.TimeDialogPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseDialogFragment;
import com.hack.dpg.shoplocator.mvp.view.TextChangeWatcher;

import javax.inject.Inject;

@SuppressWarnings("ConstantConditions")
public class TimeDialog extends BaseDialogFragment implements TimeDialogView {

    private EditText etTime;

    @Inject
    public TimeDialogPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoplocatorApplication.component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_splash_screen_time_layout, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.presenter.onViewCreated(this);
    }

    private void initViews(View view) {
        this.etTime = (EditText) view.findViewById(R.id.et_time);
        this.etTime.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onTextChanged(charSequence.toString());
            }
        });
        addListeners(view);
    }

    private void addListeners(View view) {
        (view.findViewById(R.id.btn_cancel)).setOnClickListener(this);
        (view.findViewById(R.id.btn_done)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_done:
                this.presenter.onDoneClicked(this.etTime.getText().toString());
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void dialogClose() {
        dismiss();
    }

    @Override
    public void setTimeEditText(String time) {
        this.etTime.setText(time);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

    }
}
