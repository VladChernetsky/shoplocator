package com.hack.dpg.shoplocator.mvp.presenter.splashscreen;

import com.hack.dpg.shoplocator.mvp.view.splashscreen.SplashScreenView;

public interface SplashScreenPresenter {
    void onViewCreated(SplashScreenView view);
}
