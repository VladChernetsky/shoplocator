package com.hack.dpg.shoplocator.mvp.presenter.user;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.model.user.UserDetailModel;
import com.hack.dpg.shoplocator.mvp.view.user.UserDetailView;

import java.util.List;

import javax.inject.Inject;

public class UserDetailPresenterImpl implements UserDetailPresenter {

    public interface ShopsCallback {
        void onShopsLoaded(List<Shop> userList);

        void onError(String message);
    }

    @Inject
    public UserDetailModel model;

    private UserDetailView view;
    private long userId;

    public UserDetailPresenterImpl() {
        ShoplocatorApplication
                .component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(UserDetailView userDetailView) {
        this.view = userDetailView;
        displayInfo();
    }

    @Override
    public void onUserIdReceived(long userId) {
        this.userId = userId;
    }

    @Override
    public void clickShopDetail(long shopId) {
        this.view.displayShopDetail(shopId);
    }

    @Override
    public void onUpdateEventStarted() {
        displayInfo();
    }

    private void displayInfo() {
        this.view.showProgressBar();
        this.view.displayUser(this.model.getUserById(this.userId));
        this.model.getShopsByUserId(this.userId, new ShopsCallback() {
            @Override
            public void onShopsLoaded(List<Shop> userList) {
                view.displayShops(userList);
                view.hideProgressBar();
            }

            @Override
            public void onError(String message) {
                view.showMessage(message);
                view.hideProgressBar();
            }
        });
    }
}
