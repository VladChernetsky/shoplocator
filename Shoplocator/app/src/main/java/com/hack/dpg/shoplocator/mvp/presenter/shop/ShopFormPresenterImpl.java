package com.hack.dpg.shoplocator.mvp.presenter.shop;

import android.content.Context;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopFormModel;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopFormView;
import com.hack.dpg.shoplocator.utils.HttpUtils;

import javax.inject.Inject;

import static com.hack.dpg.shoplocator.utils.HttpUtils.isNetworkAvailable;

public class ShopFormPresenterImpl implements ShopFormPresenter {

    public interface onCheckFieldsCallback {

        void onSuccess();

        void onEmptyFieldError();

        void onImageUrlError();

        void onOwnerIdError();

        void onCoordinatesError();
    }

    public interface onSaveShopCallback {
        void onSuccess();

        void onError(String message);
    }

    @Inject
    public ShopFormModel model;

    @Inject
    public Context context;

    private ShopFormView view;

    public ShopFormPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(ShopFormView view) {
        this.view = view;
        this.view.setTextBtnDone(this.model.isShopExists() ? R.string.update : R.string.create);
        this.view.setShopPropertyToEditTexts(this.model.findShop());
    }

    @Override
    public void onShopIdReceived(long shopId) {
        this.model.setShopId(shopId);
    }

    @Override
    public void onCheckFormState() {
        boolean isNetworkAvailable = isNetworkAvailable(this.context);
        if (!isNetworkAvailable) this.view.showMessage(R.string.network_error);
        this.view.enableForm(isNetworkAvailable);
    }

    @Override
    public void onTitleChanged(String title) {
        this.model.updateTitle(title);
    }

    @Override
    public void onDescriptionChanged(String description) {
        this.model.updateDescription(description);
    }

    @Override
    public void onImageChanged(String url) {
        this.model.updateImageUrl(url);
    }

    @Override
    public void onLongitudeChanged(String longitude) {
        this.model.updateLongitude(longitude);
    }

    @Override
    public void onLatitudeChanged(String latitude) {
        this.model.updateLatitude(latitude);
    }

    @Override
    public void onOwnerChanged(String ownerId) {
        this.model.updateOwner(ownerId);
    }

    @Override
    public void onCreateClicked() {
        if (!HttpUtils.isNetworkAvailable(this.context)) {
            this.view.showMessage(R.string.network_error);
            return;
        }
        this.model.isFormEntitiesValid(new onCheckFieldsCallback() {
            @Override
            public void onSuccess() {
                model.saveShop(new onSaveShopCallback() {
                    @Override
                    public void onSuccess() {
                        view.dialogDismiss();
                    }

                    @Override
                    public void onError(String message) {
                        view.showMessage(message);
                        view.dialogDismiss();
                    }
                });
            }

            @Override
            public void onEmptyFieldError() {
                view.showMessage(R.string.fields_is_not_be_empty);
            }

            @Override
            public void onImageUrlError() {
                view.showMessage(R.string.image_not_correct);
            }

            @Override
            public void onOwnerIdError() {
                view.showMessage(R.string.owner_id_error);
            }

            @Override
            public void onCoordinatesError() {
                view.showMessage(R.string.coordinates_error);
            }
        });
    }
}
