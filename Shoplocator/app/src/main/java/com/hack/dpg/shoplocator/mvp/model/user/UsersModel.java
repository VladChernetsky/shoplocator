package com.hack.dpg.shoplocator.mvp.model.user;

import com.hack.dpg.shoplocator.mvp.presenter.user.UsersPresenterImpl;

public interface UsersModel {
    void getUsers(UsersPresenterImpl.UsersCallback callback);
}
