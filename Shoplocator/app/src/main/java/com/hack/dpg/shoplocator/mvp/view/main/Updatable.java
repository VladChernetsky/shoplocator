package com.hack.dpg.shoplocator.mvp.view.main;

public interface Updatable {
    void onUpdate();
}
