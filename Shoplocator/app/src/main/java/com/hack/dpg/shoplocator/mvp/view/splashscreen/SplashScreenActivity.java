package com.hack.dpg.shoplocator.mvp.view.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.mvp.presenter.splashscreen.SplashScreenPresenter;
import com.hack.dpg.shoplocator.mvp.view.main.MainActivity;

import javax.inject.Inject;


public class SplashScreenActivity extends AppCompatActivity implements SplashScreenView {

    @Inject
    public SplashScreenPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ShoplocatorApplication.get(this)
                .getSplashScreenComponent()
                .inject(this);
        this.presenter.onViewCreated(this);
    }

    @Override
    public void displayHomePage() {
        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
        finish();
    }
}
