package com.hack.dpg.shoplocator.mvp.model.splashscreen;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SplashScreenModule;
import com.hack.dpg.shoplocator.mvp.model.settings.TimeDialogModelImpl;
import com.hack.dpg.shoplocator.utils.SharedPreferencesUtils;

import javax.inject.Inject;

public class SplashScreenModelImpl implements SplashScreenModel {

    @Inject
    public SharedPreferencesUtils preferencesUtils;

    public SplashScreenModelImpl() {
        ShoplocatorApplication.component()
                .plus(new SplashScreenModule())
                .inject(this);
    }

    @Override
    public int getSleepTimeInMilliseconds() {
        int time;
        if (this.preferencesUtils.hasIntState(TimeDialogModelImpl.PREFERENCE_TIME_TAG))
            time = this.preferencesUtils.getIntState(TimeDialogModelImpl.PREFERENCE_TIME_TAG);
        else
            time = TimeDialogModelImpl.TIME_DEFAULT;
        return time * 1000;
    }
}
