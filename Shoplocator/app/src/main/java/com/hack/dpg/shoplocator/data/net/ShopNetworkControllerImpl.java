package com.hack.dpg.shoplocator.data.net;

import com.hack.dpg.shoplocator.data.DataBaseController;
import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.pojo.ShopDeleteResponse;
import com.hack.dpg.shoplocator.data.pojo.ShopUpdateResponse;
import com.hack.dpg.shoplocator.network.RestClient;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ShopNetworkControllerImpl implements ShopNetworkController {

    @Override
    public ShopUpdateResponse updateShop(Shop shop) throws IOException {
        Call<ShopUpdateResponse> call = RestClient.getInstance().restService().updateShop(
                shop.getShopId(),
                shop.getTitle(),
                shop.getDescription(),
                shop.getImage(),
                String.valueOf(shop.getLongitude()),
                String.valueOf(shop.getLatitude()),
                shop.getUserId()
        );

        Response<ShopUpdateResponse> response = call.execute();
        return response.body();
    }

    @Override
    public List<Shop> findShopsByUserId(long userId) throws IOException {
        Call<List<Shop>> call = RestClient.getInstance().restService().loadShopsByUser(userId);
        Response<List<Shop>> response = call.execute();
        return handleResult(response.body());
    }

    @Override
    public ShopDeleteResponse removeShopById(long id) throws IOException {
        Call<ShopDeleteResponse> call = RestClient.getInstance().restService().deleteShop(id);
        Response<ShopDeleteResponse> response = call.execute();
        return response.body();
    }

    @Override
    public List<Shop> findAll() throws IOException {
        Call<List<Shop>> call = RestClient.getInstance().restService().loadShops();
        Response<List<Shop>> response = call.execute();
        return handleResult(response.body());
    }

    @Override
    public List<Shop> handleResult(List<Shop> response) {
        DataBaseController<Shop> controller = new ShopDataBaseController();
        return controller.commitEntities(response);
    }
}
