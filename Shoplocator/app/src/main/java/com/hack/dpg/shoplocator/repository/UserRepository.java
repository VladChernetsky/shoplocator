package com.hack.dpg.shoplocator.repository;

import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.network.EntityMonada;

public interface UserRepository {
    EntityMonada<User> findUsers();
}
