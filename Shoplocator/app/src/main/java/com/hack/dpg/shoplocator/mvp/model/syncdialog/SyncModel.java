package com.hack.dpg.shoplocator.mvp.model.syncdialog;

import com.hack.dpg.shoplocator.mvp.presenter.syncdialog.SyncPresenterImpl;

public interface SyncModel {
    void syncData();

    void setSyncListener(SyncPresenterImpl.SyncDataListener syncListener);
}
