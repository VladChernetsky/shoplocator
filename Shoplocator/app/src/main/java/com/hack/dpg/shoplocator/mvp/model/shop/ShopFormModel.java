package com.hack.dpg.shoplocator.mvp.model.shop;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopFormPresenterImpl;

public interface ShopFormModel {

    void isFormEntitiesValid(ShopFormPresenterImpl.onCheckFieldsCallback callback);

    void saveShop(ShopFormPresenterImpl.onSaveShopCallback callback);

    void updateTitle(String title);

    void updateDescription(String description);

    void updateImageUrl(String imageUrl);

    void updateLongitude(String longitude);

    void updateLatitude(String latitude);

    void updateOwner(String ownerId);

    Shop findShop();

    boolean isShopExists();

    void setShopId(long shopId);
}
