package com.hack.dpg.shoplocator.mvp.presenter.maps;

import com.hack.dpg.shoplocator.mvp.model.maps.ShopItemModel;
import com.hack.dpg.shoplocator.mvp.model.maps.ShopItemModelImpl;
import com.hack.dpg.shoplocator.mvp.view.maps.ShopItemView;

public class ShopItemPresenterImpl implements ShopItemPresenter {

    private ShopItemModel model;

    public ShopItemPresenterImpl() {
        this.model = new ShopItemModelImpl();
    }

    @Override
    public void onViewCreated(ShopItemView shopItemView, long id) {
        shopItemView.displayShop(this.model.getShopById(id));
    }
}
