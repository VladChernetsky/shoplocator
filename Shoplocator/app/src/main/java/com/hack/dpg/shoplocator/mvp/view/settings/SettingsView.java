package com.hack.dpg.shoplocator.mvp.view.settings;

import android.net.Uri;

import com.hack.dpg.shoplocator.mvp.view.BaseView;

public interface SettingsView extends BaseView {

    void showProgress();

    void displayTimeDialog();

    void hideProgress();

    void startEmailClient(Uri dbUri);
}
