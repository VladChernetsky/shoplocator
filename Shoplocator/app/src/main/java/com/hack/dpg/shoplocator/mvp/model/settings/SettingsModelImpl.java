package com.hack.dpg.shoplocator.mvp.model.settings;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.mvp.presenter.settings.StartEmailClientCallback;
import com.hack.dpg.shoplocator.utils.FileUtils;

import java.io.File;

public class SettingsModelImpl implements SettingsModel {

    @Override
    public void getClientChooserIntent(Context context, StartEmailClientCallback startEmailClientCallback) {
        if (FileUtils.exportDatabaseToExternalStorage(context, ShoplocatorApplication.DB_NAME)) {
            startEmailClientCallback.onSuccess(getDatabaseUri());
        } else {
            startEmailClientCallback.onError();
        }
    }

    private Uri getDatabaseUri() {
        File fileLocalDb = new File(Environment.getExternalStorageDirectory() + "/Backup_" + ShoplocatorApplication.DB_NAME);
        return Uri.fromFile(fileLocalDb);
    }
}
