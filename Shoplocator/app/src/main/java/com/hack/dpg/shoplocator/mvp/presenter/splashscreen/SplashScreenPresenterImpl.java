package com.hack.dpg.shoplocator.mvp.presenter.splashscreen;

import android.os.Handler;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SplashScreenModule;
import com.hack.dpg.shoplocator.mvp.model.splashscreen.SplashScreenModel;
import com.hack.dpg.shoplocator.mvp.view.splashscreen.SplashScreenView;

import javax.inject.Inject;

public class SplashScreenPresenterImpl implements SplashScreenPresenter {

    @Inject
    public SplashScreenModel model;

    private boolean isWorking = false;

    public SplashScreenPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new SplashScreenModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(SplashScreenView view) {
        if (isWorking)
            return;
        isWorking = true;
        new Handler().postDelayed(() -> {
            isWorking = false;
            view.displayHomePage();
        }, this.model.getSleepTimeInMilliseconds());
    }
}
