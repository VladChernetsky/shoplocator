package com.hack.dpg.shoplocator.mvp.view.settings;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.presenter.settings.SettingsPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseFragment;
import com.hack.dpg.shoplocator.xml.CustomSettingsItemView;

import javax.inject.Inject;

public class SettingsFragment extends BaseFragment implements SettingsView {

    @Inject
    public SettingsPresenter settingsPresenter;

    private ProgressBar progressBar;

    public static BaseFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoplocatorApplication
                .component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initItemsSettings(view);
        initProgressBar(view);
        addListeners(view);
        this.settingsPresenter.onViewCreated(this);
    }

    private void initItemsSettings(View view) {
        CustomSettingsItemView itemEmailSendView = (CustomSettingsItemView) view.findViewById(R.id.item_email_send);
        CustomSettingsItemView itemTimeView = (CustomSettingsItemView) view.findViewById(R.id.item_set_splash_screen_time);
        ((ImageView) itemEmailSendView.findViewById(R.id.iv_settings_icon)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_import_export));
        ((TextView) itemEmailSendView.findViewById(R.id.tv_settings_label)).setText(R.string.settings_item_email_send);
        ((ImageView) itemTimeView.findViewById(R.id.iv_settings_icon)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_access_time));
        ((TextView) itemTimeView.findViewById(R.id.tv_settings_label)).setText(R.string.settings_item_set_splash_screen_time);
    }

    private void initProgressBar(View view) {
        this.progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.progressBar.getIndeterminateDrawable().setColorFilter(getActivity().getResources().getColor(R.color.base_color), PorterDuff.Mode.MULTIPLY);
    }

    private void addListeners(View view) {
        view.findViewById(R.id.item_email_send)
                .setOnClickListener(v -> this.settingsPresenter.onEmailSendClicked(getActivity()));
        view.findViewById(R.id.item_set_splash_screen_time)
                .setOnClickListener(v -> this.settingsPresenter.onChangeSplashScreenTimeClicked());
    }

    @Override
    public void showProgress() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayTimeDialog() {
        TimeDialog dialog = new TimeDialog();
        dialog.show(getActivity().getSupportFragmentManager(), TimeDialog.class.getName());
    }

    @Override
    public void startEmailClient(Uri dbUri) {
        Intent intent = prepareEmailIntent(dbUri);
        Intent.createChooser(intent, getString(R.string.sending_email));
        startActivity(intent);
    }

    private Intent prepareEmailIntent(Uri dbUri) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Database export");
        intent.setType("application/octet-stream");
        intent.putExtra(Intent.EXTRA_STREAM, dbUri);
        intent.putExtra(android.content.Intent.EXTRA_EMAIL,
                new String[]{""});
        return intent;
    }
}
