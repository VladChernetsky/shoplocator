package com.hack.dpg.shoplocator;

import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.hack.dpg.shoplocator.di.AppModule;
import com.hack.dpg.shoplocator.di.SplashScreenModule;
import com.hack.dpg.shoplocator.di.components.ApplicationComponent;
import com.hack.dpg.shoplocator.di.components.DaggerApplicationComponent;
import com.hack.dpg.shoplocator.di.components.SplashScreenComponent;

public class ShoplocatorApplication extends com.activeandroid.app.Application {

    public static final String DB_NAME = "Shoplocator.db";

    private static ApplicationComponent component;
    private SplashScreenComponent splashScreenComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        this.initializeInjectors();
    }

    private void initializeInjectors() {
        component = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static ApplicationComponent component() {
        return component;
    }

    public SplashScreenComponent getSplashScreenComponent() {
        if (this.splashScreenComponent == null) {
            this.splashScreenComponent = component.plus(new SplashScreenModule());
        }
        return this.splashScreenComponent;
    }

    public static ShoplocatorApplication get(Context context) {
        return (ShoplocatorApplication) context.getApplicationContext();
    }
}
