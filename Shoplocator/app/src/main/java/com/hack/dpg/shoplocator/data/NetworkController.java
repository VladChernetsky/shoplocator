package com.hack.dpg.shoplocator.data;

import com.activeandroid.Model;

import java.util.List;

public interface NetworkController<T extends Model> extends DataController<T> {
    List<T> handleResult(List<T> response);
}
