package com.hack.dpg.shoplocator.mvp.model.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hack.dpg.shoplocator.data.entity.Shop;

import java.util.List;

public interface MapsModel {

    List<MarkerOptions> getShopsMarkers();

    LatLng getDisplaysShopLocation(long shopId);

    List<Shop> getShops();

    int getListPositionByShopId(List<Shop> shops, long shopId);

    String[] getShopNames();

    long getShopIdByName(String shopName);
}
