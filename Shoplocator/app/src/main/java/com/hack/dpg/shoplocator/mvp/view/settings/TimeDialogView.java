package com.hack.dpg.shoplocator.mvp.view.settings;

import android.support.v4.app.FragmentManager;
import android.view.View;

import com.hack.dpg.shoplocator.mvp.view.BaseView;

public interface TimeDialogView extends BaseView, View.OnClickListener {

    void dialogClose();

    void setTimeEditText(String time);
}
