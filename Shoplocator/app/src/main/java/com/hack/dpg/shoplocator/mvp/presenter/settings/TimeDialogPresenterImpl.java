package com.hack.dpg.shoplocator.mvp.presenter.settings;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.model.settings.TimeDialogModel;
import com.hack.dpg.shoplocator.mvp.view.settings.TimeDialogView;

import javax.inject.Inject;

public class TimeDialogPresenterImpl implements TimeDialogPresenter {

    @Inject
    public TimeDialogModel model;

    private TimeDialogView view;

    public TimeDialogPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(TimeDialogView view) {
        this.view = view;
        this.view.setTimeEditText(model.getTimeFromLocalStorage());
    }

    @Override
    public void onDoneClicked(String text) {
        try {
            this.view.setTimeEditText(text);
            this.model.saveTimeToLocalStorage(Integer.valueOf(text));
            this.view.dialogClose();
        } catch (NumberFormatException e) {
            this.view.showMessage("sorry, but you can write only numbers");
        }
    }

    @Override
    public void onTextChanged(String text) {
        if (text.length() >= 1 && Character.isDigit(text.charAt(0)) && Character.getNumericValue(text.charAt(0)) == 0)
            this.view.showMessage("first number cannot by 0");
    }
}
