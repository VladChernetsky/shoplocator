package com.hack.dpg.shoplocator.mvp.view.shop;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.view.BaseView;

public interface ShopFormView extends BaseView {

    void setTextBtnDone(int resId);

    void setShopPropertyToEditTexts(Shop shop);

    void dialogDismiss();

    void enableForm(boolean enable);
}
