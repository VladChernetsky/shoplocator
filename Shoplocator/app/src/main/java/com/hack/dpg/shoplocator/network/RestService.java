package com.hack.dpg.shoplocator.network;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.data.pojo.ShopDeleteResponse;
import com.hack.dpg.shoplocator.data.pojo.ShopUpdateResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestService {

    @GET("/api/getUsers")
    Call<List<User>> loadUsers();

    @GET("/api/getShops")
    Call<List<Shop>> loadShops();

    @GET("/api/getShopsByUser")
    Call<List<Shop>> loadShopsByUser(
            @Query("userId") long userId
    );

    @GET("/api/updateShop")
    Call<ShopUpdateResponse> updateShop(
            @Query("shopId") long shopId,
            @Query("title") String title,
            @Query("description") String description,
            @Query("image") String image,
            @Query("longitude") String longitude,
            @Query("latitude") String latitude,
            @Query("userId") long userId
    );

    @GET("/api/deleteShop")
    Call<ShopDeleteResponse> deleteShop(
            @Query("shopId") long shopId
    );
}