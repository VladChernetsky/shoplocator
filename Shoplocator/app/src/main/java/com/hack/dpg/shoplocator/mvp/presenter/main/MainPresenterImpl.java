package com.hack.dpg.shoplocator.mvp.presenter.main;

import com.hack.dpg.shoplocator.mvp.view.main.MainView;

public class MainPresenterImpl implements MainPresenter {

    private MainView view;

    @Override
    public void onViewCreated(MainView mainView) {
        this.view = mainView;
    }

    @Override
    public void onShowUsersFragmentClicked() {
        this.view.showShopsFragment();
    }

    @Override
    public void onShowShopsFragmentClicked() {
        this.view.showUsersFragment();
    }

    @Override
    public void onShowSettingsFragmentClicked() {
        this.view.showSettingsFragment();
    }
}
