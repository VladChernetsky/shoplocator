package com.hack.dpg.shoplocator.repository;

import android.content.Context;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.DataController;
import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.net.ShopNetworkControllerImpl;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.utils.HttpUtils;

import javax.inject.Inject;

public class ShopControllerFactoryImpl implements ShopControllerFactory {

    @Inject
    public Context context;

    public ShopControllerFactoryImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public DataController<Shop> provideController() {
        if (HttpUtils.isNetworkAvailable(this.context)) {
            return new ShopNetworkControllerImpl();
        } else {
            return new ShopDataBaseController();
        }
    }
}
