package com.hack.dpg.shoplocator.mvp.presenter;

public interface BaseUpdatablePresenter {
    void onUpdateEventStarted();
}
