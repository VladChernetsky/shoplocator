package com.hack.dpg.shoplocator.mvp.presenter.settings;

import android.net.Uri;

public interface StartEmailClientCallback {
    void onSuccess(Uri uri);

    void onError();
}
