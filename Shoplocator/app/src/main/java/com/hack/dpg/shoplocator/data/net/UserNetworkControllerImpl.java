package com.hack.dpg.shoplocator.data.net;

import com.hack.dpg.shoplocator.data.DataBaseController;
import com.hack.dpg.shoplocator.data.db.UserDataBaseController;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.network.RestClient;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class UserNetworkControllerImpl implements UserNetworkController {

    @Override
    public List<User> findAll() throws IOException {
        Call<List<User>> call = RestClient.getInstance().restService().loadUsers();
        Response<List<User>> response = call.execute();
        return handleResult(response.body());
    }

    @Override
    public List<User> handleResult(List<User> response) {
        DataBaseController<User> controller = new UserDataBaseController();
        return controller.commitEntities(response);
    }
}
