package com.hack.dpg.shoplocator.mvp.view;

import android.text.Editable;
import android.text.TextWatcher;

public abstract class TextChangeWatcher implements TextWatcher {

    public abstract void onWatchedTextChanged(CharSequence charSequence);

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        onWatchedTextChanged(charSequence);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
