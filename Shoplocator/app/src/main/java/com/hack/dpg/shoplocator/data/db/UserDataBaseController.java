package com.hack.dpg.shoplocator.data.db;

import android.support.annotation.NonNull;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.hack.dpg.shoplocator.data.entity.User;

public class UserDataBaseController extends AbstractDataBaseController<User> {

    public UserDataBaseController() {
        super(User.class);
    }

    @Override
    protected boolean isEntityExist(User model) {
        String condition = User.FIELD_USER_ID + "=" + model
                .getUserId();
        return findByCondition(condition) != null && findByCondition(condition).size() > 0;
    }

    @Override
    protected User updateEntity(User entity) {
        new Update(User.class).set(
                getPrepareSet(),
                entity.getName(),
                entity.getAddress(),
                entity.getPicture(),
                entity.getPhone())
                .where(User.FIELD_USER_ID + "=?", entity.getUserId())
                .execute();
        return new Select().from(User.class).where(User.FIELD_USER_ID + "=?", entity.getUserId()).executeSingle();
    }

    @NonNull
    private String getPrepareSet() {
        return User.FIELD_USER_NAME + "=?, "
                + User.FIELD_ADDRESS + "=?, " +
                User.FIELD_PICTURE + "=?, " +
                User.FIELD_PHONE + "=?";
    }
}
