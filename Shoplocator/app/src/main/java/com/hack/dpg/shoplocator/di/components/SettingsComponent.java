package com.hack.dpg.shoplocator.di.components;

import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.model.settings.TimeDialogModelImpl;
import com.hack.dpg.shoplocator.mvp.model.syncdialog.SyncModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.settings.SettingsPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.settings.TimeDialogPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.syncdialog.SyncPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.settings.SettingsFragment;
import com.hack.dpg.shoplocator.mvp.view.settings.TimeDialog;
import com.hack.dpg.shoplocator.mvp.view.syncdialog.SyncDialog;

import dagger.Subcomponent;

@Subcomponent(modules = SettingsModule.class)
public interface SettingsComponent {

    void inject(SettingsFragment view);

    void inject(SettingsPresenterImpl presenter);

    void inject(TimeDialog dialog);

    void inject(TimeDialogPresenterImpl presenter);

    void inject(TimeDialogModelImpl model);

    void inject(SyncDialog dialog);

    void inject(SyncPresenterImpl presenter);

    void inject(SyncModelImpl model);
}
