package com.hack.dpg.shoplocator.mvp.presenter.main;

import com.hack.dpg.shoplocator.mvp.view.main.MainView;

public interface MainPresenter {

    void onViewCreated(MainView mainView);

    void onShowUsersFragmentClicked();

    void onShowShopsFragmentClicked();

    void onShowSettingsFragmentClicked();
}
