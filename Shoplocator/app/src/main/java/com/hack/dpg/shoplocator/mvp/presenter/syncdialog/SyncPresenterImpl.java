package com.hack.dpg.shoplocator.mvp.presenter.syncdialog;

import android.content.Context;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.model.syncdialog.SyncModel;
import com.hack.dpg.shoplocator.mvp.view.syncdialog.SyncView;
import com.hack.dpg.shoplocator.utils.HttpUtils;

import javax.inject.Inject;

public class SyncPresenterImpl implements SyncPresenter {

    @Inject
    public Context context;

    @Inject
    public SyncModel model;

    private SyncView view;

    public interface SyncDataListener {

        void onSuccess();

        void onFailure(String errorMessage);
    }

    public SyncPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new SettingsModule())
                .inject(this);

        this.model.setSyncListener(new SyncDataListener() {
            @Override
            public void onSuccess() {
                view.onSyncSuccess();
            }

            @Override
            public void onFailure(String errorMessage) {
                view.onSyncFailure(errorMessage);
            }
        });
    }

    @Override
    public void onViewCreated(SyncView syncView) {
        this.view = syncView;
    }

    @Override
    public void onStartSyncData() {
        if (HttpUtils.isNetworkAvailable(this.context)) {
            this.model.syncData();
        } else {
            this.view.onSyncFailure(this.context.getString(R.string.network_error));
        }
    }
}
