package com.hack.dpg.shoplocator.mvp.presenter.user;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.model.user.UsersModel;
import com.hack.dpg.shoplocator.mvp.view.user.UsersView;

import java.util.List;

import javax.inject.Inject;

public class UsersPresenterImpl implements UsersPresenter {

    public interface UsersCallback {
        void onUsersLoaded(List<User> userList);

        void onError(String message);
    }

    @Inject
    public UsersModel userModel;

    private UsersView view;

    public UsersPresenterImpl() {
        ShoplocatorApplication
                .component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(UsersView view) {
        this.view = view;
        onUsersLoad();
    }

    @Override
    public void onItemClicked(User user) {
        this.view.displayUserDetail(user.getId());
    }

    @Override
    public void onUpdateEventStarted() {
        onUsersLoad();
    }

    private void onUsersLoad() {
        this.view.showProgressBar();
        this.userModel.getUsers(new UsersCallback() {
            @Override
            public void onUsersLoaded(List<User> userList) {
                view.hideProgressBar();
                view.displayUsers(userList);
            }

            @Override
            public void onError(String message) {
                view.hideProgressBar();
                view.showMessage(message);
            }
        });
    }
}
