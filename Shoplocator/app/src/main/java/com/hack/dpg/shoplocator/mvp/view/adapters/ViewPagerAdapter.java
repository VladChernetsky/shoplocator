package com.hack.dpg.shoplocator.mvp.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.view.maps.ShopItemFragment;

import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Shop> shops;

    public ViewPagerAdapter(FragmentManager fm, List<Shop> shops) {
        super(fm);
        this.shops = shops;
    }

    @Override
    public Fragment getItem(int position) {
        return ShopItemFragment.newInstance(this.shops.get(position).getId());
    }

    @Override
    public int getCount() {
        return this.shops.size();
    }
}
