package com.hack.dpg.shoplocator.mvp.presenter.maps;

import com.hack.dpg.shoplocator.mvp.view.maps.ShopItemView;

public interface ShopItemPresenter {
    void onViewCreated(ShopItemView shopItemView, long id);
}
