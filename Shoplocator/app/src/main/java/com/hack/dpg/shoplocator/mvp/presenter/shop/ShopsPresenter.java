package com.hack.dpg.shoplocator.mvp.presenter.shop;

import com.hack.dpg.shoplocator.mvp.presenter.BaseUpdatablePresenter;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopsView;

import java.util.List;

public interface ShopsPresenter extends BaseUpdatablePresenter {

    void onViewCreated(ShopsView shopsView);

    void onShopsRemoveClicked(List<Long> shopsIds);

    void onShopFormCalled(long shopId);

    void onItemSelected(int position);

    void onSelectedItemCountChanged(int count);
}
