package com.hack.dpg.shoplocator.data.pojo;

public class ShopDeleteResponse {

    private Response response;

    public ShopDeleteResponse(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public class Response {
        private int code;
        private String message;

        public Response(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}
