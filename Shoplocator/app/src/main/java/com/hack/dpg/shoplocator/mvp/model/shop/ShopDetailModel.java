package com.hack.dpg.shoplocator.mvp.model.shop;

import com.hack.dpg.shoplocator.data.entity.Shop;

public interface ShopDetailModel {
    Shop getShopById(long shopId);
}
