package com.hack.dpg.shoplocator.xml;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.hack.dpg.shoplocator.R;

public class ShopLayoutInputItem extends LinearLayout {
    public ShopLayoutInputItem(Context context) {
        super(context);
        addInnerView(context);
    }

    public ShopLayoutInputItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        addInnerView(context);
    }

    public ShopLayoutInputItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addInnerView(context);
    }

    private void addInnerView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.shop_form_layout_item, this, false);
        addView(view);
    }
}
