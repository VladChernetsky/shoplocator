package com.hack.dpg.shoplocator.repository;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.network.EntityMonada;

public interface ShopRepository {
    EntityMonada<Shop> findShops();

    EntityMonada<Shop> findShopsByUserId(long userId);

    boolean delete(long shopId);

    String save(Shop shop);
}
