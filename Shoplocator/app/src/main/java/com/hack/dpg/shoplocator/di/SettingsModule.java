package com.hack.dpg.shoplocator.di;

import com.hack.dpg.shoplocator.mvp.model.settings.SettingsModel;
import com.hack.dpg.shoplocator.mvp.model.settings.SettingsModelImpl;
import com.hack.dpg.shoplocator.mvp.model.settings.TimeDialogModel;
import com.hack.dpg.shoplocator.mvp.model.settings.TimeDialogModelImpl;
import com.hack.dpg.shoplocator.mvp.model.syncdialog.SyncModel;
import com.hack.dpg.shoplocator.mvp.model.syncdialog.SyncModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.settings.TimeDialogPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.settings.TimeDialogPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.settings.SettingsPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.settings.SettingsPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.syncdialog.SyncPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.syncdialog.SyncPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {

    @Provides
    SettingsModel provideSettingsModel() {
        return new SettingsModelImpl();
    }

    @Provides
    SettingsPresenter provideSettingsPresenter() {
        return new SettingsPresenterImpl();
    }

    @Provides
    TimeDialogPresenter provideDialogPresenter() {
        return new TimeDialogPresenterImpl();
    }

    @Provides
    TimeDialogModel provideDialogModel() {
        return new TimeDialogModelImpl();
    }

    @Provides
    SyncModel provideSyncModel() {
        return new SyncModelImpl();
    }

    @Provides
    SyncPresenter provideSyncPresenter() {
        return new SyncPresenterImpl();
    }
}
