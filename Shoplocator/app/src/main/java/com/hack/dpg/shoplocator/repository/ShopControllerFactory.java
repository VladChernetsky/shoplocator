package com.hack.dpg.shoplocator.repository;

import com.hack.dpg.shoplocator.data.DataController;
import com.hack.dpg.shoplocator.data.entity.Shop;

public interface ShopControllerFactory {
    DataController<Shop> provideController();
}
