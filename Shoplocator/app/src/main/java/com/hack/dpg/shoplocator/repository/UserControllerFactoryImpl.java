package com.hack.dpg.shoplocator.repository;

import android.content.Context;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.DataController;
import com.hack.dpg.shoplocator.data.db.UserDataBaseController;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.data.net.UserNetworkControllerImpl;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.utils.HttpUtils;

import javax.inject.Inject;

public class UserControllerFactoryImpl implements UserControllerFactory {

    @Inject
    public Context context;

    public UserControllerFactoryImpl() {
        ShoplocatorApplication.component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    public DataController<User> provideUserController() {
        if (HttpUtils.isNetworkAvailable(this.context)) {
            return new UserNetworkControllerImpl();
        } else {
            return new UserDataBaseController();
        }
    }
}
