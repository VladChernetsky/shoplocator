package com.hack.dpg.shoplocator.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileUtils {

    public static boolean exportDatabaseToExternalStorage(Context context, String dbName) {
        String currentDBPath = "/data/" + context.getPackageName() + "/databases/" + dbName;
        File currentDB = new File(Environment.getDataDirectory(), currentDBPath);
        File backupDB = new File(Environment.getExternalStorageDirectory(), "Backup_" + dbName);
        return transferFile(currentDB, backupDB);
    }

    private static boolean transferFile(File currentDB, File backupDB) {
        FileChannel source;
        FileChannel destination;
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
