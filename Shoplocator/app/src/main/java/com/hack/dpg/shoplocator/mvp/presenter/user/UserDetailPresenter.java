package com.hack.dpg.shoplocator.mvp.presenter.user;

import com.hack.dpg.shoplocator.mvp.presenter.BaseUpdatablePresenter;
import com.hack.dpg.shoplocator.mvp.view.user.UserDetailView;

public interface UserDetailPresenter extends BaseUpdatablePresenter {

    void onViewCreated(UserDetailView userDetailView);

    void onUserIdReceived(long userId);

    void clickShopDetail(long shopId);
}
