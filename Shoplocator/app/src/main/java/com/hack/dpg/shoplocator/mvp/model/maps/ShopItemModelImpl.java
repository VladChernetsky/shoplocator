package com.hack.dpg.shoplocator.mvp.model.maps;

import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;

public class ShopItemModelImpl implements ShopItemModel {

    @Override
    public Shop getShopById(long shopId) {
        ShopDataBaseController controller = new ShopDataBaseController();
        return controller.findEntityById(shopId);
    }
}
