package com.hack.dpg.shoplocator.di;

import com.hack.dpg.shoplocator.mvp.model.maps.MapsModel;
import com.hack.dpg.shoplocator.mvp.model.maps.MapsModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.maps.MapsPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.maps.MapsPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class MapsModule {

    @Provides
    MapsPresenter provideMapsPresenter() {
        return new MapsPresenterImpl();
    }

    @Provides
    MapsModel provideMapsModel() {
        return new MapsModelImpl();
    }
}
