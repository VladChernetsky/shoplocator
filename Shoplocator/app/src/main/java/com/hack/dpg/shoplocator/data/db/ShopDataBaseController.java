package com.hack.dpg.shoplocator.data.db;

import android.support.annotation.NonNull;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.hack.dpg.shoplocator.data.entity.Shop;

@SuppressWarnings("unchecked")
public class ShopDataBaseController extends AbstractDataBaseController<Shop> {

    public ShopDataBaseController() {
        super(Shop.class);
    }

    @Override
    protected boolean isEntityExist(Shop model) {
        String condition = Shop.FIELD_TITLE + " LIKE '%" + model.getTitle() +
                "%' AND " + Shop.FIELD_USER_ID + "=" + model.getUserId();
        return findByCondition(condition) != null && findByCondition(condition).size() > 0;
    }

    @Override
    public Shop updateEntity(Shop shop) {
        String criteria = Shop.FIELD_SHOP_ID + "=" + shop.getShopId();
        new Update(Shop.class).set(
                getPrepareSetCriteria(),
                shop.getTitle(),
                shop.getDescription(),
                shop.getImage(),
                shop.getLongitude(),
                shop.getLatitude(),
                shop.getShopId(),
                shop.getUserId())
                .where(criteria)
                .execute();
        return new Select().from(Shop.class).where(criteria).executeSingle();
    }

    @NonNull
    private String getPrepareSetCriteria() {
        return Shop.FIELD_TITLE + "=?, "
                + Shop.FIELD_DESCRIPTION + "=?, "
                + Shop.FIELD_IMAGE + "=?, "
                + Shop.FIELD_LONGITUDE + "=?, "
                + Shop.FIELD_LATITUDE + "=?, "
                + Shop.FIELD_SHOP_ID + "=?, "
                + Shop.FIELD_USER_ID + "=?";
    }

    public Shop findShopByName(String title) {
        String condition = Shop.FIELD_TITLE + " LIKE '%" + title + "%'";
        return findByCondition(condition).get(0);
    }
}
