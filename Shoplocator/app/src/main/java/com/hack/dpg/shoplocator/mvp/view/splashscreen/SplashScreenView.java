package com.hack.dpg.shoplocator.mvp.view.splashscreen;

import com.hack.dpg.shoplocator.mvp.view.BaseView;

/**
 * Created by vladch on 02.08.16.
 */
public interface SplashScreenView {
    void displayHomePage();
}
