package com.hack.dpg.shoplocator.di;

import com.hack.dpg.shoplocator.mvp.model.splashscreen.SplashScreenModel;
import com.hack.dpg.shoplocator.mvp.model.splashscreen.SplashScreenModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.splashscreen.SplashScreenPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.splashscreen.SplashScreenPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashScreenModule {

    @Provides
    @Singleton
    SplashScreenPresenter provideSplashScreenPresenter() {
        return new SplashScreenPresenterImpl();
    }

    @Provides
    SplashScreenModel provideSplashScreenModel() {
        return new SplashScreenModelImpl();
    }
}
