
package com.hack.dpg.shoplocator.mvp.view.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.mvp.presenter.main.MainPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.main.MainPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.settings.SettingsFragment;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopDetailFragment;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopsFragment;
import com.hack.dpg.shoplocator.mvp.view.syncdialog.SyncDialog;
import com.hack.dpg.shoplocator.mvp.view.user.UserDetailFragment;
import com.hack.dpg.shoplocator.mvp.view.user.UsersFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {

    private final String SAVED_FRAGMENT_TAG = "saved_fragment_tag";
    private final int REQUEST_CODE = 1004;
    private final int DELAY_CLOSE_NAV_DRAWER = 300;
    private String savedFragmentTag;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private List<Updatable> updatableFragments = new ArrayList<>();
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mainPresenter = new MainPresenterImpl();
        if (savedInstanceState != null)
            this.savedFragmentTag = savedInstanceState.getString(SAVED_FRAGMENT_TAG);

        initToolbar();
        initNavigationDrawer();
        showStartScreen();
        checkReadWritePermissions();
        this.mainPresenter.onViewCreated(this);
    }

    private void initToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    private void initNavigationDrawer() {
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_navigation);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, this.drawerLayout,
                this.toolbar, R.string.open, R.string.close);

        this.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (navigationView != null)
            addDrawerListener(navigationView);
    }

    private void showStartScreen() {
        if (getSupportFragmentManager().findFragmentByTag(savedFragmentTag) == null) {
            displayFragment(UsersFragment.getInstance(), R.id.item_content_frame, true);
        } else {
            displayFragment(getSupportFragmentManager().findFragmentByTag(savedFragmentTag),
                    R.id.item_content_frame, true);
        }
    }

    private void checkReadWritePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE);
        }
    }

    private void addDrawerListener(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(item -> {
            this.drawerLayout.closeDrawers();
            clearDetailView();
            new Handler().postDelayed(() -> {
                switch (item.getItemId()) {
                    case R.id.shops_navigation_menu_item:
                        this.mainPresenter.onShowShopsFragmentClicked();
                        break;
                    case R.id.users_navigation_menu_item:
                        this.mainPresenter.onShowUsersFragmentClicked();
                        break;
                    case R.id.settings_navigation_menu_item:
                        this.mainPresenter.onShowSettingsFragmentClicked();
                        break;
                    default:
                        break;
                }
            }, DELAY_CLOSE_NAV_DRAWER);
            return true;
        });
    }

    private void clearDetailView() {
        if (isMasterDetail()) {
            removeFragmentByTag(ShopDetailFragment.class.getName());
            removeFragmentByTag(UserDetailFragment.class.getName());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length != 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showShopsFragment() {
        displayFragment(ShopsFragment.getInstance(), R.id.item_content_frame, true);
    }

    @Override
    public void showUsersFragment() {
        displayFragment(UsersFragment.getInstance(), R.id.item_content_frame, true);
    }

    @Override
    public void showSettingsFragment() {
        displayFragment(SettingsFragment.newInstance(), R.id.item_content_frame, true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVED_FRAGMENT_TAG, savedFragmentTag);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync_data:
                showSyncDataDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void clickShopDetail(long shopId) {
        displayFragment(ShopDetailFragment.getInstance(shopId), isMasterDetail() ?
                R.id.detail_content_frame : R.id.item_content_frame, false);
    }

    @Override
    public void clickUserDetail(long userId) {
        displayFragment(UserDetailFragment.getInstance(userId), isMasterDetail() ?
                R.id.detail_content_frame : R.id.item_content_frame, false);
    }

    @Override
    public void onDbStorageUpdated() {
        notifyFragments();
    }

    @Override
    public void subscribeFragment(Updatable updatable) {
        this.updatableFragments.add(updatable);
    }

    @Override
    public void unSubscribeFragment(Updatable updatable) {
        this.updatableFragments.remove(updatable);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            finish();
        super.onBackPressed();
    }

    private void showSyncDataDialog() {
        DialogFragment dialogFragment = SyncDialog.newInstance(getString(R.string.sync_string));
        dialogFragment.show(getSupportFragmentManager(), SyncDialog.class.getName());
    }

    private boolean isMasterDetail() {
        return findViewById(R.id.detail_content_frame) != null;
    }

    private void displayFragment(Fragment fragment, int frameId, boolean save) {
        if (save) this.savedFragmentTag = fragment.getClass().getCanonicalName();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(frameId, fragment, fragment.getClass().getName());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void removeFragmentByTag(String fragmentTag) {
        if (getSupportFragmentManager().findFragmentByTag(fragmentTag) != null)
            removeFragment(getSupportFragmentManager().findFragmentByTag(fragmentTag));
    }

    private void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    private void notifyFragments() {
        for (Updatable fragment : this.updatableFragments)
            fragment.onUpdate();
    }
}
