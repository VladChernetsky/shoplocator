package com.hack.dpg.shoplocator.di.components;

import com.hack.dpg.shoplocator.di.SplashScreenModule;
import com.hack.dpg.shoplocator.mvp.model.splashscreen.SplashScreenModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.splashscreen.SplashScreenPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.splashscreen.SplashScreenActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {SplashScreenModule.class})
public interface SplashScreenComponent {

    void inject(SplashScreenActivity activity);

    void inject(SplashScreenPresenterImpl presenter);

    void inject(SplashScreenModelImpl model);
}
