package com.hack.dpg.shoplocator.mvp.view.maps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.presenter.maps.ShopItemPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.maps.ShopItemPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.BaseFragment;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopDetailFragment;
import com.squareup.picasso.Picasso;

public class ShopItemFragment extends BaseFragment implements ShopItemView {

    private ShopItemPresenter shopItemPresenter;
    private TextView tvName;
    private TextView tvDescription;
    private ImageView ivShopImage;

    public static ShopItemFragment newInstance(long shopId) {
        ShopItemFragment shopItemFragment = new ShopItemFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ShopDetailFragment.SHOP_ID_TAG, shopId);
        shopItemFragment.setArguments(bundle);
        return shopItemFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.shopItemPresenter = new ShopItemPresenterImpl();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_pager_shop_item_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.tvName = (TextView) view.findViewById(R.id.tv_shop_name);
        this.tvDescription = (TextView) view.findViewById(R.id.tv_shop_description);
        this.ivShopImage = (ImageView) view.findViewById(R.id.iv_shop_picture);
        this.shopItemPresenter.onViewCreated(this, getArguments().getLong(ShopDetailFragment.SHOP_ID_TAG));
    }

    @Override
    public void displayShop(Shop shop) {
        this.tvName.setText(shop.getTitle());
        this.tvDescription.setText(shop.getDescription());
        Picasso.with(getActivity()).load(shop.getImage()).placeholder(getActivity().getResources().getDrawable(R.drawable.ic_panorama)).fit().into(ivShopImage);
    }
}
