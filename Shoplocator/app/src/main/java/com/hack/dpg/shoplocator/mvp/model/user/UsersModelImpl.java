package com.hack.dpg.shoplocator.mvp.model.user;

import android.os.AsyncTask;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.presenter.user.UsersPresenterImpl;
import com.hack.dpg.shoplocator.network.EntityMonada;
import com.hack.dpg.shoplocator.repository.UserRepository;

import javax.inject.Inject;

@SuppressWarnings("Convert2streamapi")
public class UsersModelImpl implements UsersModel {

    @Inject
    UserRepository userRepository;

    public UsersModelImpl() {
        ShoplocatorApplication.component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    public void getUsers(UsersPresenterImpl.UsersCallback callback) {
        new AsyncTask<Void, Void, EntityMonada<User>>() {
            @Override
            protected EntityMonada<User> doInBackground(Void... params) {
                return userRepository.findUsers();
            }

            @Override
            protected void onPostExecute(EntityMonada<User> entityMonada) {
                super.onPostExecute(entityMonada);
                if (entityMonada.hasError()) {
                    callback.onError(entityMonada.getException().getMessage());
                } else {
                    callback.onUsersLoaded(entityMonada.getEntity());
                }
            }
        }.execute();
    }
}
