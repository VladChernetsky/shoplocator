package com.hack.dpg.shoplocator.mvp.presenter.shop;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopDetailModel;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopDetailView;

import javax.inject.Inject;

public class ShopDetailPresenterImpl implements ShopDetailPresenter {

    @Inject
    public ShopDetailModel model;

    private ShopDetailView shopDetailView;
    private long shopId;

    public ShopDetailPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(ShopDetailView shopDetailView) {
        this.shopDetailView = shopDetailView;
        this.shopDetailView.displayShop(this.model.getShopById(this.shopId));
    }

    @Override
    public void onShopIdReceived(long shopId) {
        this.shopId = shopId;
    }

    @Override
    public void onShowMapClicked(long shopId) {
        this.shopDetailView.startMapsActivity(shopId);
    }

    @Override
    public void onUpdateEventStarted() {
        this.shopDetailView.displayShop(this.model.getShopById(this.shopId));
    }
}
