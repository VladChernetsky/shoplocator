package com.hack.dpg.shoplocator.di;

import com.hack.dpg.shoplocator.mvp.model.user.UserDetailModel;
import com.hack.dpg.shoplocator.mvp.model.user.UserDetailModelImpl;
import com.hack.dpg.shoplocator.mvp.model.user.UsersModel;
import com.hack.dpg.shoplocator.mvp.model.user.UsersModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.user.UserDetailPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.user.UserDetailPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.user.UsersPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.user.UsersPresenterImpl;
import com.hack.dpg.shoplocator.repository.UserControllerFactory;
import com.hack.dpg.shoplocator.repository.UserControllerFactoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @Provides
    UsersPresenter provideUserPresenter() {
        return new UsersPresenterImpl();
    }

    @Provides
    UsersModel provideUserModel() {
        return new UsersModelImpl();
    }

    @Provides
    UserDetailPresenter provideUserDetailPresenter() {
        return new UserDetailPresenterImpl();
    }

    @Provides
    UserDetailModel provideUserDetailModel() {
        return new UserDetailModelImpl();
    }

    @Provides
    UserControllerFactory provideUserControllerFactory() {
        return new UserControllerFactoryImpl();
    }
}
