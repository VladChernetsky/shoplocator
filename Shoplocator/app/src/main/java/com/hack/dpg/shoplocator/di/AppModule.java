package com.hack.dpg.shoplocator.di;

import android.app.Application;
import android.content.Context;

import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.db.UserDataBaseController;
import com.hack.dpg.shoplocator.repository.ShopRepository;
import com.hack.dpg.shoplocator.repository.ShopRepositoryImpl;
import com.hack.dpg.shoplocator.repository.UserRepository;
import com.hack.dpg.shoplocator.repository.UserRepositoryImpl;
import com.hack.dpg.shoplocator.utils.SharedPreferencesUtils;
import com.hack.dpg.shoplocator.utils.SharedPreferencesUtilsImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    public Context provideApplicationContext() {
        return this.application.getApplicationContext();
    }

    @Provides
    SharedPreferencesUtils provideSharedPreferences(Context context) {
        return new SharedPreferencesUtilsImpl(context);
    }

    @Provides
    ShopDataBaseController provideShopDataBaseController() {
        return new ShopDataBaseController();
    }

    @Provides
    UserDataBaseController provideUserDataBaseController() {
        return new UserDataBaseController();
    }

    @Provides
    UserRepository provideUserRepository() {
        return new UserRepositoryImpl();
    }

    @Provides
    ShopRepository provideShopRepository() {
        return new ShopRepositoryImpl();
    }
}
