package com.hack.dpg.shoplocator.mvp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.view.user.UserDetailFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserShopsAdapter extends RecyclerView.Adapter<UserShopsAdapter.UserShopViewHolder> {

    private Context context;
    private List<Shop> shopList;
    private UserDetailFragment.ShopDetailClickListener shopDetailClickListener;

    public UserShopsAdapter(Context context, List<Shop> shopList, UserDetailFragment.ShopDetailClickListener shopDetailClickListener) {
        this.context = context;
        this.shopList = shopList;
        this.shopDetailClickListener = shopDetailClickListener;
    }

    @Override
    public UserShopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_view_shop_item_layout, parent, false);
        return new UserShopViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserShopViewHolder holder, int position) {
        Shop shop = this.shopList.get(position);
        holder.tvShopName.setText(shop.getTitle());
        Picasso.with(this.context)
                .load(shop.getImage())
                .placeholder(this.context.getResources().getDrawable(R.drawable.ic_panorama))
                .fit().centerCrop()
                .into(holder.ivShopImage);
    }

    @Override
    public int getItemCount() {
        return this.shopList.size();
    }

    @Override
    public long getItemId(int position) {
        return this.shopList.get(position).getId();
    }

    public void updateShops(List<Shop> shops) {
        this.shopList.clear();
        this.shopList.addAll(shops);
        notifyDataSetChanged();
    }

    public class UserShopViewHolder extends RecyclerView.ViewHolder {

        private TextView tvShopName;
        private ImageView ivShopImage;

        public UserShopViewHolder(View itemView) {
            super(itemView);
            this.tvShopName = (TextView) itemView.findViewById(R.id.tv_shop_name);
            this.ivShopImage = (ImageView) itemView.findViewById(R.id.iv_shop_picture);
            itemView.setOnClickListener(v -> shopDetailClickListener.onShopDetailClick(
                    shopList.get(getAdapterPosition()).getId()));
        }
    }
}
