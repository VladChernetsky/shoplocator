package com.hack.dpg.shoplocator.mvp.view.maps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.MapsModule;
import com.hack.dpg.shoplocator.mvp.presenter.maps.MapsPresenter;
import com.hack.dpg.shoplocator.mvp.view.adapters.ViewPagerAdapter;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopDetailFragment;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import javax.inject.Inject;

public class MapsActivity extends AppCompatActivity implements MapsView, OnMapReadyCallback, ViewPager.OnPageChangeListener {
    private static final String ACTIVE_SHOP_ID = "active_shop";

    private GoogleMap map;

    @Inject
    public MapsPresenter mapsPresenter;
    private long activeShopId = 0;
    private ViewPager viewPager;
    private MaterialSearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ShoplocatorApplication.component()
                .plus(new MapsModule())
                .inject(this);
        initToolbar();
        getExistActiveShop();
        setupMapsFragment();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.mapsPresenter.onViewCreated(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private void getExistActiveShop() {
        if (getIntent() != null && getIntent().getExtras() != null)
            this.activeShopId = getIntent().getExtras().getLong(ShopDetailFragment.SHOP_ID_TAG);
    }

    private void setupMapsFragment() {
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ACTIVE_SHOP_ID, this.activeShopId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        this.activeShopId = state.getLong(ACTIVE_SHOP_ID);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        this.mapsPresenter.onMapReady(this.activeShopId);
    }

    @Override
    public void initViewPager(List<Shop> shops) {
        this.viewPager = (ViewPager) findViewById(R.id.vp_shops);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), shops);
        this.viewPager.setAdapter(viewPagerAdapter);
        this.viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void displayMarkers(List<MarkerOptions> markerList) {
        for (MarkerOptions markerOptions : markerList)
            this.map.addMarker(markerOptions);
    }

    @Override
    public void moveCameraToMarker(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(12)
                .build();
        this.map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1500, null);
    }

    @Override
    public void setViewPagerPositionByShop(int position) {
        this.viewPager.setCurrentItem(position);
    }

    @Override
    public void onBackPressed() {
        if (this.searchView.isSearchOpen()) {
            this.searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void initSearchView(String[] shopNames) {
        this.searchView = (MaterialSearchView) findViewById(R.id.search_view);
        this.searchView.setEllipsize(true);
        this.searchView.setSuggestions(shopNames);
        this.searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                displaySearchResult(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        this.searchView.setOnItemClickListener((adapterView, view, i, l) -> {
            displaySearchResult(((ListView) adapterView).getAdapter().getItem(i).toString());
        });
    }

    @Override
    public void saveActiveShop(long shopId) {
        this.activeShopId = shopId;
    }

    private void displaySearchResult(String shopName) {
        this.searchView.dismissSuggestions();
        this.searchView.closeSearch();
        this.mapsPresenter.onShopFounded(shopName);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        this.mapsPresenter.onViewedShopChanged(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps_toolbar_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        this.searchView.setMenuItem(item);

        return true;
    }
}
