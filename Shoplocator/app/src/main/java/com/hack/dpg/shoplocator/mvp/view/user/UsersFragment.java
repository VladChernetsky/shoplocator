package com.hack.dpg.shoplocator.mvp.view.user;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.presenter.user.UsersPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseUpdatableFragment;
import com.hack.dpg.shoplocator.mvp.view.FragmentDetailClickListener;
import com.hack.dpg.shoplocator.mvp.view.adapters.UsersAdapter;
import com.hack.dpg.shoplocator.mvp.view.main.MainActivity;
import com.hack.dpg.shoplocator.xml.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class UsersFragment extends BaseUpdatableFragment implements UsersView {

    @Inject
    public UsersPresenter presenter;

    private FragmentDetailClickListener fragmentDetailClickListener;
    private UsersAdapter usersAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    public interface UserDetailClickListener {
        void onUserClick(User user);
    }

    public static Fragment getInstance() {
        return new UsersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoplocatorApplication
                .component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    protected void onUpdateFragment() {
        this.presenter.onUpdateEventStarted();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.fragmentDetailClickListener = (MainActivity) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        this.presenter.onViewCreated(this);
    }

    private void initViews(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_user_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.usersAdapter = new UsersAdapter(getActivity(), new ArrayList<>(), this.userDetailClickListener);
        recyclerView.setAdapter(this.usersAdapter);
        initSwipeRefreshLayout(view);
    }

    private void initSwipeRefreshLayout(View view) {
        this.swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        this.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        this.swipeRefreshLayout.setOnRefreshListener(() -> presenter.onUpdateEventStarted());
    }

    @Override
    public void displayUsers(List<User> userList) {
        this.usersAdapter.updateUsers(userList);
        this.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void displayUserDetail(long userId) {
        this.fragmentDetailClickListener.clickUserDetail(userId);
    }

    @Override
    public void showProgressBar() {
        this.dialogView.show(getActivity(), getFragmentManager());
    }

    @Override
    public void hideProgressBar() {
        if (this.dialogView != null)
            this.dialogView.dismissDialog();
    }

    private UserDetailClickListener userDetailClickListener = user -> this.presenter.onItemClicked(user);
}
