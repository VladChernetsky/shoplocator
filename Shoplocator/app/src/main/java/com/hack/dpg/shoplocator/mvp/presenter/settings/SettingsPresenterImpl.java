package com.hack.dpg.shoplocator.mvp.presenter.settings;

import android.content.Context;
import android.net.Uri;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.model.settings.SettingsModel;
import com.hack.dpg.shoplocator.mvp.view.settings.SettingsView;

import javax.inject.Inject;

public class SettingsPresenterImpl implements SettingsPresenter, StartEmailClientCallback {

    @Inject
    public SettingsModel model;

    private SettingsView settingsView;

    public SettingsPresenterImpl() {
        ShoplocatorApplication
                .component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(SettingsView settingsView) {
        this.settingsView = settingsView;
    }

    @Override
    public void onEmailSendClicked(Context context) {
        this.settingsView.showProgress();
        this.model.getClientChooserIntent(context, this);
    }

    @Override
    public void onChangeSplashScreenTimeClicked() {
        this.settingsView.displayTimeDialog();
    }

    @Override
    public void onSuccess(Uri uri) {
        this.settingsView.hideProgress();
        this.settingsView.startEmailClient(uri);
    }

    @Override
    public void onError() {
        this.settingsView.hideProgress();
        this.settingsView.showMessage("export db error :(");
    }
}
