package com.hack.dpg.shoplocator.mvp.presenter.settings;

import com.hack.dpg.shoplocator.mvp.view.settings.TimeDialogView;

public interface TimeDialogPresenter {

    void onViewCreated(TimeDialogView view);

    void onDoneClicked(String text);

    void onTextChanged(String text);
}
