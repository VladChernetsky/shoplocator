package com.hack.dpg.shoplocator.mvp.presenter.shop;

import com.hack.dpg.shoplocator.mvp.view.shop.ShopFormView;

public interface ShopFormPresenter {

    void onViewCreated(ShopFormView view);

    void onShopIdReceived(long shopId);

    void onCheckFormState();

    void onTitleChanged(String title);

    void onDescriptionChanged(String description);

    void onImageChanged(String url);

    void onLongitudeChanged(String longitude);

    void onLatitudeChanged(String latitude);

    void onOwnerChanged(String ownerId);

    void onCreateClicked();
}
