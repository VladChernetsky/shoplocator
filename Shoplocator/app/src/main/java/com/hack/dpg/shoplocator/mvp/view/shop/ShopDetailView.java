package com.hack.dpg.shoplocator.mvp.view.shop;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.view.BaseView;

public interface ShopDetailView extends BaseView {

    void displayShop(Shop shop);

    void startMapsActivity(long shopId);
}
