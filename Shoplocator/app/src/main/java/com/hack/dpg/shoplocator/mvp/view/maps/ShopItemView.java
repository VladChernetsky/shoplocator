package com.hack.dpg.shoplocator.mvp.view.maps;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.view.BaseView;

public interface ShopItemView extends BaseView {

    void displayShop(Shop shop);
}
