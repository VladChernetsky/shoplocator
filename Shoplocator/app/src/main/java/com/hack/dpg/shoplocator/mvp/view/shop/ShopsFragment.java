package com.hack.dpg.shoplocator.mvp.view.shop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseUpdatableFragment;
import com.hack.dpg.shoplocator.mvp.view.FragmentDetailClickListener;
import com.hack.dpg.shoplocator.mvp.view.adapters.ShopsAdapter;
import com.hack.dpg.shoplocator.xml.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ShopsFragment extends BaseUpdatableFragment implements ShopsView {

    private final int REQUEST_CODE = 1003;

    @Inject
    public ShopsPresenter presenter;

    private FragmentDetailClickListener fragmentDetailClickListener;
    private ShopsAdapter shopsAdapter;
    private ActionMode actionMode;
    private ActionModeCallback actionModeCallback = new ActionModeCallback();
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    protected void onUpdateFragment() {
        this.presenter.onUpdateEventStarted();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.fragmentDetailClickListener = (FragmentDetailClickListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shop_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView(view);
        this.presenter.onViewCreated(this);
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_shop_list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.shopsAdapter = new ShopsAdapter(getActivity(), new ArrayList<>(), this);
        recyclerView.setAdapter(this.shopsAdapter);
        initSwipeRefreshLayout(view);
    }

    private void initSwipeRefreshLayout(View view) {
        this.swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        this.swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        this.swipeRefreshLayout.setOnRefreshListener(() -> this.presenter.onUpdateEventStarted());
    }

    @Override
    public void onItemClicked(int position) {
        if (this.actionMode != null) {
            this.presenter.onItemSelected(position);
            this.presenter.onSelectedItemCountChanged(this.shopsAdapter.getSelectedItemCount());
        } else {
            this.fragmentDetailClickListener.clickShopDetail(this.shopsAdapter.getItemId(position));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_actions_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_item:
                this.presenter.onShopFormCalled(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onLongItemClicked(int position) {
        if (this.actionMode == null) {
            this.actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this.actionModeCallback);
            AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
            if (appCompatActivity.getSupportActionBar() != null) {
                appCompatActivity.getSupportActionBar().hide();
            }
        }
        this.presenter.onItemSelected(position);
        this.presenter.onSelectedItemCountChanged(this.shopsAdapter.getSelectedItemCount());
        return true;
    }

    @Override
    public void displayShops(List<Shop> shopList) {
        this.shopsAdapter.updateShops(shopList);
        this.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void displayShopForm(long shopId) {
        ShopFormDialog shopFormDialog = ShopFormDialog.newInstance(shopId);
        shopFormDialog.setTargetFragment(this, REQUEST_CODE);
        shopFormDialog.show(getActivity().getSupportFragmentManager(), ShopFormDialog.class.getName());
    }

    @Override
    public void finishActionMode() {
        this.actionMode.finish();
    }

    @Override
    public void setActionModeTitle(String title) {
        this.actionMode.setTitle(title);
        this.actionMode.invalidate();
    }

    @Override
    public void toggleSelectedItem(int position) {
        this.shopsAdapter.toggleSelection(position);
    }

    @Override
    public void showProgressBar() {
        this.dialogView.show(getActivity(), getFragmentManager());
    }

    @Override
    public void hideProgressBar() {
        this.dialogView.dismissDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == ShopFormDialog.RESULT_CODE) {
            this.presenter.onUpdateEventStarted();
        }
    }

    public static Fragment getInstance() {
        return new ShopsFragment();
    }

    private class ActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            if (shopsAdapter.getSelectedItemCount() <= 1)
                mode.getMenuInflater().inflate(R.menu.menu_action_full_mode, menu);
            else
                mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            mode.getMenu().clear();
            onCreateActionMode(mode, menu);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.shop_remove:
                    presenter.onShopsRemoveClicked(
                            shopsAdapter.getShopsIdsByPositions(shopsAdapter.getSelectedItems()));
                    mode.finish();
                    return true;
                case R.id.shop_update:
                    presenter.onShopFormCalled(shopsAdapter.getItemId(shopsAdapter.getSelectedItems().get(0)));
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            shopsAdapter.clearSelection();
            actionMode = null;
            AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
            if (appCompatActivity.getSupportActionBar() != null) {
                appCompatActivity.getSupportActionBar().show();
            }
        }
    }
}
