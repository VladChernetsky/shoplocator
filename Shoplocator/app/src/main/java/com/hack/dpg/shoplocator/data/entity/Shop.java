package com.hack.dpg.shoplocator.data.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "shop")
public class Shop extends Model {
    public static final String FIELD_SHOP_ID = "shopId";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_IMAGE = "image";
    public static final String FIELD_LONGITUDE = "longitude";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_USER_ID = "userId";

    @Column(name = FIELD_SHOP_ID)
    private long shopId = 0;

    @Column(name = FIELD_TITLE)
    private String title;

    @Column(name = FIELD_DESCRIPTION)
    private String description;

    @Column(name = FIELD_IMAGE)
    private String image;

    @Column(name = FIELD_LONGITUDE)
    private double longitude;

    @Column(name = FIELD_LATITUDE)
    private double latitude;

    @Column(name = FIELD_USER_ID, index = true)
    private long userId;

    public Shop() {
        super();
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public long getUserId() {
        return userId;
    }

    public long getShopId() {
        return shopId;
    }

    public static class Builder {
        private long userId;
        private long shopId;
        private double longitude;
        private double latitude;
        private String title;
        private String description;
        private String image;

        public Builder setUserId(long userId) {
            this.userId = userId;
            return this;
        }

        public Builder setShopId(long shopId) {
            this.shopId = shopId;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setImage(String image) {
            this.image = image;
            return this;
        }

        public Builder setLongitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setLatitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Shop create() {
            Shop shop = new Shop();
            shop.userId = userId;
            shop.shopId = shopId;
            shop.title = title;
            shop.description = description;
            shop.image = image;
            shop.longitude = longitude;
            shop.latitude = latitude;
            return shop;
        }
    }
}
