package com.hack.dpg.shoplocator.di.components;

import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.model.user.UserDetailModelImpl;
import com.hack.dpg.shoplocator.mvp.model.user.UsersModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.user.UserDetailPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.user.UsersPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.user.UserDetailFragment;
import com.hack.dpg.shoplocator.mvp.view.user.UsersFragment;
import com.hack.dpg.shoplocator.repository.UserControllerFactoryImpl;
import com.hack.dpg.shoplocator.repository.UserRepositoryImpl;

import dagger.Subcomponent;

@Subcomponent(modules = UserModule.class)
public interface UserComponent {
    void inject(UsersFragment fragment);

    void inject(UsersPresenterImpl presenter);

    void inject(UserDetailFragment fragment);

    void inject(UserDetailPresenterImpl presenter);

    void inject(UsersModelImpl model);

    void inject(UserDetailModelImpl model);

    void inject(UserControllerFactoryImpl factory);

    void inject(UserRepositoryImpl repository);
}
