package com.hack.dpg.shoplocator.di.components;

import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopDetailModelImpl;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopFormModelImpl;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopsModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopDetailPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopFormPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopDetailFragment;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopFormDialog;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopsFragment;
import com.hack.dpg.shoplocator.repository.ShopControllerFactoryImpl;
import com.hack.dpg.shoplocator.repository.ShopRepositoryImpl;

import dagger.Subcomponent;

@Subcomponent(modules = ShopModule.class)
public interface ShopComponent {
    void inject(ShopsFragment fragment);

    void inject(ShopsPresenterImpl presenter);

    void inject(ShopDetailFragment fragment);

    void inject(ShopDetailPresenterImpl presenter);

    void inject(ShopsModelImpl model);

    void inject(ShopDetailModelImpl model);

    void inject(ShopFormDialog view);

    void inject(ShopFormPresenterImpl presenter);

    void inject(ShopControllerFactoryImpl factory);

    void inject(ShopRepositoryImpl repository);

    void inject(ShopFormModelImpl model);
}
