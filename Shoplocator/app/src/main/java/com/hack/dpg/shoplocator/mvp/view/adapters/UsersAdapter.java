package com.hack.dpg.shoplocator.mvp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.mvp.view.user.UsersFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {

    private Context context;
    private List<User> users;
    private UsersFragment.UserDetailClickListener userDetailClickListener;

    public UsersAdapter(Context context, List<User> users, UsersFragment.UserDetailClickListener userDetailClickListener) {
        this.context = context;
        this.users = users;
        this.userDetailClickListener = userDetailClickListener;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.item_user_list, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = this.users.get(position);
        holder.tvName.setText(user.getName());
        holder.tvAddress.setText(user.getAddress());
        Picasso.with(context)
                .load(user.getPicture())
                .placeholder(this.context.getResources().getDrawable(R.drawable.ic_panorama))
                .fit()
                .centerCrop()
                .into(holder.ivUserPhoto);
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    @Override
    public long getItemId(int position) {
        return this.users.get(position).getId();
    }

    public void updateUsers(List<User> users) {
        this.users.clear();
        this.users.addAll(users);
        notifyDataSetChanged();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvAddress;
        private ImageView ivUserPhoto;

        public UserViewHolder(View itemView) {
            super(itemView);
            this.tvName = (TextView) itemView.findViewById(R.id.tv_user_name);
            this.tvAddress = (TextView) itemView.findViewById(R.id.tv_user_address);
            this.ivUserPhoto = (ImageView) itemView.findViewById(R.id.iv_user_photo);
            itemView.setOnClickListener(v -> userDetailClickListener.onUserClick(
                    users.get(getAdapterPosition())));
        }
    }

}
