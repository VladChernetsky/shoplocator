package com.hack.dpg.shoplocator.mvp.model.shop;

import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenterImpl;

import java.util.List;

public interface ShopsModel {

    void getShops(ShopsPresenterImpl.ShopCallback callback);

    void deleteShops(List<Long> shopsIds, Runnable callback);

    void handleCountSelectedItems(int count, ShopsPresenterImpl.SelectedItemCountListener itemCountListener);
}
