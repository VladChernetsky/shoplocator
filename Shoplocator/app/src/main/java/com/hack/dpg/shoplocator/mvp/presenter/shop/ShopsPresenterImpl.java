package com.hack.dpg.shoplocator.mvp.presenter.shop;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopsModel;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopsView;

import java.util.List;

import javax.inject.Inject;

public class ShopsPresenterImpl implements ShopsPresenter {

    public interface SelectedItemCountListener {
        void closeActionMode();

        void getSelectedItemCount(int countSelectedItems);
    }

    public interface ShopCallback {
        void onShopsLoaded(List<Shop> shops);

        void onLoadError(String message);
    }

    @Inject
    public ShopsModel model;

    private ShopsView view;

    public ShopsPresenterImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public void onViewCreated(ShopsView shopsView) {
        this.view = shopsView;
        loadShops();
    }

    @Override
    public void onShopsRemoveClicked(List<Long> shopsIds) {
        this.model.deleteShops(shopsIds, this::loadShops);
    }

    @Override
    public void onShopFormCalled(long shopId) {
        this.view.displayShopForm(shopId);
    }

    @Override
    public void onItemSelected(int position) {
        this.view.toggleSelectedItem(position);
    }

    @Override
    public void onSelectedItemCountChanged(int count) {
        this.model.handleCountSelectedItems(count, new SelectedItemCountListener() {
            @Override
            public void closeActionMode() {
                view.finishActionMode();
            }

            @Override
            public void getSelectedItemCount(int countSelectedItems) {
                view.setActionModeTitle(String.valueOf(countSelectedItems));
            }
        });
    }

    @Override
    public void onUpdateEventStarted() {
        loadShops();
    }

    private void loadShops() {
        this.view.showProgressBar();
        this.model.getShops(new ShopCallback() {
            @Override
            public void onShopsLoaded(List<Shop> shops) {
                view.displayShops(shops);
                view.hideProgressBar();
            }

            @Override
            public void onLoadError(String message) {
                view.showMessage(message);
                view.hideProgressBar();
            }
        });
    }
}
