package com.hack.dpg.shoplocator.mvp.model.user;

import android.os.AsyncTask;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.db.UserDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.presenter.user.UserDetailPresenterImpl;
import com.hack.dpg.shoplocator.network.EntityMonada;
import com.hack.dpg.shoplocator.repository.ShopRepository;

import javax.inject.Inject;

public class UserDetailModelImpl implements UserDetailModel {

    @Inject
    UserDataBaseController controller;

    @Inject
    ShopRepository repository;

    public UserDetailModelImpl() {
        ShoplocatorApplication.component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    public User getUserById(long userId) {
        return controller.findEntityById(userId);
    }

    @Override
    public void getShopsByUserId(long userId, UserDetailPresenterImpl.ShopsCallback callback) {
        new AsyncTask<Void, Void, EntityMonada<Shop>>() {
            @Override
            protected EntityMonada<Shop> doInBackground(Void... params) {
                return repository.findShopsByUserId(userId);
            }

            @Override
            protected void onPostExecute(EntityMonada<Shop> entityMonada) {
                super.onPostExecute(entityMonada);
                if (entityMonada.hasError()) {
                    callback.onError(entityMonada.getException().getMessage());
                } else {
                    callback.onShopsLoaded(entityMonada.getEntity());
                }
            }
        }.execute();
    }
}
