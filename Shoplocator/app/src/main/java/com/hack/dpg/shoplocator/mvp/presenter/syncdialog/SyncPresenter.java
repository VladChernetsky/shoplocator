package com.hack.dpg.shoplocator.mvp.presenter.syncdialog;

import com.hack.dpg.shoplocator.mvp.view.syncdialog.SyncView;

public interface SyncPresenter {
    void onViewCreated(SyncView syncView);

    void onStartSyncData();
}
