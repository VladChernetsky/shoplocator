package com.hack.dpg.shoplocator.mvp.presenter.settings;

import android.content.Context;

import com.hack.dpg.shoplocator.mvp.view.settings.SettingsView;

public interface SettingsPresenter {

    void onViewCreated(SettingsView settingsView);

    void onEmailSendClicked(Context context);

    void onChangeSplashScreenTimeClicked();
}
