package com.hack.dpg.shoplocator.data.pojo;

public class ShopUpdateResponse {
    private Response response;

    public void setResponse(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    @Override
    public String toString() {
        return response.getMessage();
    }

    public class Response {

        private String message;
        private long shopId;
        private int code;

        public Response(String message, long shopId, int code) {
            this.message = message;
            this.shopId = shopId;
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public long getShopId() {
            return shopId;
        }

        public int getCode() {
            return code;
        }

        @Override
        public String toString() {
            return "ClassPojo [message = " + message + ", shopId = " + shopId + ", code = " + code + "]";
        }
    }
}