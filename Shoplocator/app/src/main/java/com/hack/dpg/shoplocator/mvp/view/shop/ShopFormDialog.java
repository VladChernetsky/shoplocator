package com.hack.dpg.shoplocator.mvp.view.shop;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopFormPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseDialogFragment;
import com.hack.dpg.shoplocator.mvp.view.TextChangeWatcher;
import com.hack.dpg.shoplocator.xml.ShopLayoutInputItem;

import javax.inject.Inject;

public class ShopFormDialog extends BaseDialogFragment implements ShopFormView {

    public static final int RESULT_CODE = 1;
    public static final String SHOP_ID = "SHOP_ID";

    @Inject
    public ShopFormPresenter presenter;

    private EditText etTitle;
    private EditText etDescription;
    private EditText etImage;
    private EditText etOwner;
    private EditText etLatitude;
    private EditText etLongitude;
    private Button btnDone;

    public static ShopFormDialog newInstance(long shopId) {
        ShopFormDialog shopFormDialog = new ShopFormDialog();
        Bundle bundle = new Bundle();
        bundle.putLong(SHOP_ID, shopId);
        shopFormDialog.setArguments(bundle);
        return shopFormDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
        this.presenter.onShopIdReceived(getArguments().getLong(SHOP_ID, 0));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.shop_form_layout, container, false);
        initViews(view);
        addListeners();
        return view;
    }

    private void initViews(View view) {
        ShopLayoutInputItem itemTitle = (ShopLayoutInputItem) view.findViewById(R.id.shop_item_title);
        ((TextInputLayout) itemTitle.findViewById(R.id.text_input_layout)).setHint(getString(R.string.title));
        this.etTitle = (EditText) itemTitle.findViewById(R.id.et_shop_item);

        ShopLayoutInputItem itemDescription = (ShopLayoutInputItem) view.findViewById(R.id.shop_item_description);
        ((TextInputLayout) itemDescription.findViewById(R.id.text_input_layout)).setHint(getString(R.string.description));
        this.etDescription = (EditText) itemDescription.findViewById(R.id.et_shop_item);

        ShopLayoutInputItem itemImage = (ShopLayoutInputItem) view.findViewById(R.id.shop_item_image);
        ((TextInputLayout) itemImage.findViewById(R.id.text_input_layout)).setHint(getString(R.string.image));
        this.etImage = (EditText) itemImage.findViewById(R.id.et_shop_item);

        ShopLayoutInputItem itemLongitude = (ShopLayoutInputItem) view.findViewById(R.id.shop_item_longitude);
        ((TextInputLayout) itemLongitude.findViewById(R.id.text_input_layout)).setHint(getString(R.string.longitude));
        this.etLongitude = (EditText) itemLongitude.findViewById(R.id.et_shop_item);

        ShopLayoutInputItem itemLatitude = (ShopLayoutInputItem) view.findViewById(R.id.shop_item_latitude);
        ((TextInputLayout) itemLatitude.findViewById(R.id.text_input_layout)).setHint(getString(R.string.latitude));
        this.etLatitude = (EditText) itemLatitude.findViewById(R.id.et_shop_item);

        ShopLayoutInputItem itemOwner = (ShopLayoutInputItem) view.findViewById(R.id.shop_item_owner);
        ((TextInputLayout) itemOwner.findViewById(R.id.text_input_layout)).setHint(getString(R.string.owner));
        this.etOwner = (EditText) itemOwner.findViewById(R.id.et_shop_item);

        this.btnDone = (Button) view.findViewById(R.id.btn_done);
    }

    private void addListeners() {
        this.btnDone.setOnClickListener(v -> this.presenter.onCreateClicked());

        this.etTitle.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onTitleChanged(charSequence.toString());
            }
        });

        this.etDescription.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onDescriptionChanged(charSequence.toString());
            }
        });

        this.etImage.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onImageChanged(charSequence.toString());
            }
        });

        this.etLongitude.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onLongitudeChanged(charSequence.toString());
            }
        });

        this.etLatitude.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onLatitudeChanged(charSequence.toString());
            }
        });

        this.etOwner.addTextChangedListener(new TextChangeWatcher() {
            @Override
            public void onWatchedTextChanged(CharSequence charSequence) {
                presenter.onOwnerChanged(charSequence.toString());
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.presenter.onViewCreated(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.presenter.onCheckFormState();
    }

    @Override
    public void setTextBtnDone(int resId) {
        this.btnDone.setText(resId);
    }

    @Override
    public void setShopPropertyToEditTexts(Shop shop) {
        this.etTitle.setText(shop.getTitle());
        this.etDescription.setText(shop.getDescription());
        this.etImage.setText(shop.getImage());
        this.etLatitude.setText(String.valueOf(shop.getLatitude()));
        this.etLongitude.setText(String.valueOf(shop.getLongitude()));
        this.etOwner.setText(String.valueOf(shop.getUserId()));
    }

    @Override
    public void dialogDismiss() {
        int resultCode = 1;
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, null);
        dismiss();
    }

    @Override
    public void enableForm(boolean enable) {
        etTitle.setEnabled(enable);
        etDescription.setEnabled(enable);
        etLatitude.setEnabled(enable);
        etLongitude.setEnabled(enable);
        etImage.setEnabled(enable);
        etOwner.setEnabled(enable);
    }
}
