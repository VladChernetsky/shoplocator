package com.hack.dpg.shoplocator.data.net;

import com.hack.dpg.shoplocator.data.NetworkController;
import com.hack.dpg.shoplocator.data.entity.User;

public interface UserNetworkController extends NetworkController<User> {
}
