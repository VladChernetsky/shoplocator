package com.hack.dpg.shoplocator.di;

import com.hack.dpg.shoplocator.data.net.ShopNetworkController;
import com.hack.dpg.shoplocator.data.net.ShopNetworkControllerImpl;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopDetailModel;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopDetailModelImpl;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopFormModel;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopFormModelImpl;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopsModel;
import com.hack.dpg.shoplocator.mvp.model.shop.ShopsModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopDetailPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopDetailPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopFormPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopFormPresenterImpl;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenter;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenterImpl;
import com.hack.dpg.shoplocator.repository.ShopControllerFactory;
import com.hack.dpg.shoplocator.repository.ShopControllerFactoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ShopModule {

    @Provides
    ShopsPresenter provideShopsPresenter() {
        return new ShopsPresenterImpl();
    }

    @Provides
    ShopsModel provideShopsModel() {
        return new ShopsModelImpl();
    }

    @Provides
    ShopDetailPresenter provideShopDetailPresenter() {
        return new ShopDetailPresenterImpl();
    }

    @Provides
    ShopDetailModel provideShopDetailModel() {
        return new ShopDetailModelImpl();
    }

    @Provides
    ShopFormModel provideShopFormModel() {
        return new ShopFormModelImpl();
    }

    @Provides
    ShopFormPresenter provideShopFormPresenter() {
        return new ShopFormPresenterImpl();
    }

    @Provides
    ShopControllerFactory provideControllerFactory() {
        return new ShopControllerFactoryImpl();
    }

    @Provides
    ShopNetworkController provideNetworkController() {
        return new ShopNetworkControllerImpl();
    }
}
