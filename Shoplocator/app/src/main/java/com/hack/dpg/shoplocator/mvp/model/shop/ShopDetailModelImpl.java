package com.hack.dpg.shoplocator.mvp.model.shop;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.ShopModule;

import javax.inject.Inject;

public class ShopDetailModelImpl implements ShopDetailModel {

    @Inject
    ShopDataBaseController controller;

    public ShopDetailModelImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public Shop getShopById(long shopId) {
        return controller.findEntityById(shopId);
    }
}