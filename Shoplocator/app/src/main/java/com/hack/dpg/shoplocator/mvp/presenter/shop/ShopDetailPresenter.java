package com.hack.dpg.shoplocator.mvp.presenter.shop;

import com.hack.dpg.shoplocator.mvp.presenter.BaseUpdatablePresenter;
import com.hack.dpg.shoplocator.mvp.view.shop.ShopDetailView;

public interface ShopDetailPresenter extends BaseUpdatablePresenter {
    void onViewCreated(ShopDetailView shopDetailView);

    void onShowMapClicked(long shopId);

    void onShopIdReceived(long shopId);
}
