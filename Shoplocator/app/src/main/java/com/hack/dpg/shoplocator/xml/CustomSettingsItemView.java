package com.hack.dpg.shoplocator.xml;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.hack.dpg.shoplocator.R;

public class CustomSettingsItemView extends LinearLayout {

    public CustomSettingsItemView(Context context) {
        super(context);
        addCustomLayout();
    }

    public CustomSettingsItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        addCustomLayout();
    }

    public CustomSettingsItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addCustomLayout();
    }

    private void addCustomLayout() {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(getContext())
                .inflate(R.layout.settings_view_item, null);
        this.addView(view);
    }
}
