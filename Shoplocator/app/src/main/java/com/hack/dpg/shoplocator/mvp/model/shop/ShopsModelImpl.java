package com.hack.dpg.shoplocator.mvp.model.shop;

import android.os.AsyncTask;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenterImpl;
import com.hack.dpg.shoplocator.network.EntityMonada;
import com.hack.dpg.shoplocator.repository.ShopRepository;

import java.util.List;

import javax.inject.Inject;

@SuppressWarnings("Convert2streamapi")
public class ShopsModelImpl implements ShopsModel {

    @Inject
    ShopRepository shopRepository;

    public ShopsModelImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public void getShops(ShopsPresenterImpl.ShopCallback callback) {
        new AsyncTask<Void, Void, EntityMonada<Shop>>() {

            @Override
            protected EntityMonada<Shop> doInBackground(Void... params) {
                return shopRepository.findShops();
            }

            @Override
            protected void onPostExecute(EntityMonada<Shop> entityMonada) {
                super.onPostExecute(entityMonada);
                if (entityMonada.hasError()) {
                    callback.onLoadError(entityMonada.getException().getMessage());
                } else {
                    callback.onShopsLoaded(entityMonada.getEntity());
                }
            }
        }.execute();
    }

    @Override
    public void deleteShops(List<Long> shopsIds, Runnable callback) {
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                for (Long id : shopsIds) {
                    if (!shopRepository.delete(id)) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                callback.run();
            }
        }.execute();
    }

    @Override
    public void handleCountSelectedItems(int count, ShopsPresenterImpl.SelectedItemCountListener itemCountListener) {
        if (count == 0) {
            itemCountListener.closeActionMode();
        } else {
            itemCountListener.getSelectedItemCount(count);
        }
    }
}
