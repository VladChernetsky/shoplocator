package com.hack.dpg.shoplocator.mvp.model.settings;

import android.content.Context;

import com.hack.dpg.shoplocator.mvp.presenter.settings.StartEmailClientCallback;

public interface SettingsModel {
    void getClientChooserIntent(Context context, StartEmailClientCallback startEmailClientCallback);
}
