package com.hack.dpg.shoplocator.mvp.view.user;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.mvp.view.BaseView;

import java.util.List;

public interface UserDetailView extends BaseView {

    void displayUser(User user);

    void displayShops(List<Shop> shopList);

    void displayShopDetail(long shopId);

    void showProgressBar();

    void hideProgressBar();
}
