package com.hack.dpg.shoplocator.data.net;

import com.hack.dpg.shoplocator.data.NetworkController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.pojo.ShopDeleteResponse;
import com.hack.dpg.shoplocator.data.pojo.ShopUpdateResponse;

import java.io.IOException;
import java.util.List;

public interface ShopNetworkController extends NetworkController<Shop> {

    ShopUpdateResponse updateShop(Shop shop) throws IOException;

    List<Shop> findShopsByUserId(long userId) throws IOException;

    ShopDeleteResponse removeShopById(long id) throws IOException;
}
