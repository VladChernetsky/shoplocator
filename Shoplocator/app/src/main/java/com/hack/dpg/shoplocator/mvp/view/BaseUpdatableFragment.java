package com.hack.dpg.shoplocator.mvp.view;

import android.content.Context;

import com.hack.dpg.shoplocator.mvp.view.main.FragmentSubscriber;
import com.hack.dpg.shoplocator.mvp.view.main.Updatable;

public abstract class BaseUpdatableFragment extends BaseFragment implements Updatable {

    protected FragmentSubscriber fragmentSubscriber;

    protected abstract void onUpdateFragment();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.fragmentSubscriber = (FragmentSubscriber) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        this.fragmentSubscriber.subscribeFragment(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        this.fragmentSubscriber.unSubscribeFragment(this);
    }

    @Override
    public void onUpdate() {
        onUpdateFragment();
    }
}
