package com.hack.dpg.shoplocator.mvp.view.user;

import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.mvp.view.BaseView;

import java.util.List;

public interface UsersView extends BaseView {

    void displayUsers(List<User> userList);

    void displayUserDetail(long userId);

    void showProgressBar();

    void hideProgressBar();
}
