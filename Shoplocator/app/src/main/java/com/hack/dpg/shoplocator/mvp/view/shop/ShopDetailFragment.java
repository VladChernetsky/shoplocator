package com.hack.dpg.shoplocator.mvp.view.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopDetailPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseUpdatableFragment;
import com.hack.dpg.shoplocator.mvp.view.maps.MapsActivity;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class ShopDetailFragment extends BaseUpdatableFragment implements ShopDetailView, View.OnClickListener {

    public static final String SHOP_ID_TAG = "shop_id";

    @Inject
    public ShopDetailPresenter presenter;

    private TextView tvName;
    private TextView tvDescription;
    private ImageView ivPicture;

    public static Fragment getInstance(long shopId) {
        Fragment fragment = new ShopDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(SHOP_ID_TAG, shopId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
        this.presenter.onShopIdReceived(getArguments().getLong(SHOP_ID_TAG));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shop_detail, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        addListeners(view);
        this.presenter.onViewCreated(this);
    }

    private void findViews(View view) {
        this.tvName = (TextView) view.findViewById(R.id.tv_shop_name);
        this.tvDescription = (TextView) view.findViewById(R.id.tv_shop_description);
        this.ivPicture = (ImageView) view.findViewById(R.id.iv_shop_picture);
    }

    private void addListeners(View view) {
        view.findViewById(R.id.iv_location).setOnClickListener(this);
    }

    @Override
    protected void onUpdateFragment() {
        this.presenter.onUpdateEventStarted();
    }

    @Override
    public void displayShop(Shop shop) {
        this.tvName.setText(shop.getTitle());
        this.tvDescription.setText(shop.getDescription());
        Picasso.with(getActivity()).load(shop.getImage()).placeholder(getActivity().getResources().getDrawable(R.drawable.ic_panorama)).into(this.ivPicture);
    }

    @Override
    public void startMapsActivity(long shopId) {
        Intent intent = new Intent(getActivity(), MapsActivity.class);
        intent.putExtra(SHOP_ID_TAG, shopId);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_location:
                this.presenter.onShowMapClicked(getArguments().getLong(SHOP_ID_TAG));
                break;
            default:
                break;
        }
    }
}
