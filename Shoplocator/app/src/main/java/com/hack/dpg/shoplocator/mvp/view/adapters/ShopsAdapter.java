package com.hack.dpg.shoplocator.mvp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.xml.FontTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ShopsAdapter extends SelectableAdapter<ShopsAdapter.ViewHolder> {

    private List<Shop> shops;

    private Context context;

    private ShopClickListener shopClickListener;

    public interface ShopClickListener {

        void onItemClicked(int position);

        boolean onLongItemClicked(int position);
    }

    public ShopsAdapter(Context context, List<Shop> shops, ShopClickListener shopClickListener) {
        this.shops = shops;
        this.shopClickListener = shopClickListener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.shop_list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Shop shop = this.shops.get(position);
        if (shop == null)
            return;
        holder.tvShopName.setText(shop.getTitle());
        holder.tvShopDescription.setText(shop.getDescription());
        holder.selectedView.setVisibility(isSelected(position) ? View.VISIBLE : View.GONE);
        Picasso.with(context).load(shop.getImage()).placeholder(context.getResources().getDrawable(R.drawable.ic_panorama)).fit().centerCrop().into(holder.ivShopIcon);
    }

    @Override
    public long getItemId(int position) {
        return this.shops.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return this.shops.size();
    }

    public void updateShops(List<Shop> shops) {
        this.shops.clear();
        this.shops.addAll(shops);
        notifyDataSetChanged();
    }

    @SuppressWarnings("Convert2streamapi")
    public List<Long> getShopsIdsByPositions(List<Integer> positions) {
        List<Long> shopsIds = new ArrayList<>();
        for (Integer position : positions)
            shopsIds.add(getItemId(position));
        return shopsIds;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {

        private FontTextView tvShopName;
        private FontTextView tvShopDescription;
        private ImageView ivShopIcon;
        private View selectedView;

        ViewHolder(View view) {
            super(view);
            tvShopName = (FontTextView) view.findViewById(R.id.tv_shop_name);
            tvShopDescription = (FontTextView) view.findViewById(R.id.tv_shop_description);
            ivShopIcon = (ImageView) view.findViewById(R.id.iv_shop_picture);
            selectedView = view.findViewById(R.id.view_selected);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            shopClickListener.onItemClicked(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return shopClickListener != null && shopClickListener.onLongItemClicked(getAdapterPosition());
        }
    }
}
