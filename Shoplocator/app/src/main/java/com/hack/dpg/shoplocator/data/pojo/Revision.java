package com.hack.dpg.shoplocator.data.pojo;

/**
 * Created by vladch on 06.08.16.
 */
public class Revision {

    public static final String REVISION_PREFERENCE = "revision";

    private int revision;

    public Revision(int revision) {
        this.revision = revision;
    }

    public int getRevision() {
        return revision;
    }
}
