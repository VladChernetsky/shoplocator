package com.hack.dpg.shoplocator.di.components;

import com.hack.dpg.shoplocator.di.AppModule;
import com.hack.dpg.shoplocator.di.MapsModule;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.di.SplashScreenModule;
import com.hack.dpg.shoplocator.di.UserModule;

import javax.inject.Singleton;

import dagger.Component;


@Component(modules = {AppModule.class})
@Singleton
public interface ApplicationComponent {
    SettingsComponent plus(SettingsModule module);

    UserComponent plus(UserModule module);

    ShopComponent plus(ShopModule module);

    MapComponents plus(MapsModule module);

    SplashScreenComponent plus(SplashScreenModule module);
}
