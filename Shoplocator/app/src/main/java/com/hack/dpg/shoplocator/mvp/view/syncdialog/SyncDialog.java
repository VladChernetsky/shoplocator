package com.hack.dpg.shoplocator.mvp.view.syncdialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.presenter.syncdialog.SyncPresenter;

import javax.inject.Inject;

public class SyncDialog extends DialogFragment implements SyncView {

    public interface DbStorageUpdateListener {
        void onDbStorageUpdated();
    }

    public static final String TITLE_ARG = "title";

    @Inject
    public SyncPresenter syncPresenter;

    private DbStorageUpdateListener dbStorageUpdateListener;

    public static DialogFragment newInstance(String title) {
        SyncDialog syncDialog = new SyncDialog();
        Bundle args = new Bundle();

        args.putString(TITLE_ARG, title);
        syncDialog.setArguments(args);
        return syncDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoplocatorApplication.component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.sync_dialog_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(view);

        this.syncPresenter.onViewCreated(this);
        this.syncPresenter.onStartSyncData();

        return alertDialogBuilder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.dbStorageUpdateListener = (DbStorageUpdateListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onSyncSuccess() {
        getActivity().runOnUiThread(() -> {
            Toast.makeText(getActivity(), R.string.success_sync, Toast.LENGTH_SHORT).show();
            this.dbStorageUpdateListener.onDbStorageUpdated();
            dismiss();
        });
    }

    @Override
    public void onSyncFailure(String errorMessage) {
        getActivity().runOnUiThread(() -> {
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            dismiss();
        });
    }
}
