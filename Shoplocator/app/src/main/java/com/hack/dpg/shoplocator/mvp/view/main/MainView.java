package com.hack.dpg.shoplocator.mvp.view.main;

import com.hack.dpg.shoplocator.mvp.view.FragmentDetailClickListener;
import com.hack.dpg.shoplocator.mvp.view.syncdialog.SyncDialog;

public interface MainView extends FragmentDetailClickListener, SyncDialog.DbStorageUpdateListener, FragmentSubscriber {

    void showShopsFragment();

    void showSettingsFragment();

    void showUsersFragment();
}
