package com.hack.dpg.shoplocator.mvp.view.syncdialog;

public interface SyncView {

    void onSyncSuccess();

    void onSyncFailure(String errorMessage);
}
