package com.hack.dpg.shoplocator.data.db;

import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.hack.dpg.shoplocator.data.DataBaseController;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Convert2streamapi")
abstract class AbstractDataBaseController<T extends Model> implements DataBaseController<T> {

    private Class<T> subModel;

    AbstractDataBaseController(Class<T> subModel) {
        this.subModel = subModel;
    }

    @Override
    public List<T> findAll() {
        return new Select().from(this.subModel).execute();
    }

    @Override
    public List<T> findByCondition(String condition) {
        return new Select().from(this.subModel)
                .where(condition)
                .execute();
    }

    @Override
    public T findEntityById(long id) {
        return new Select().from(this.subModel).where("id=?", id).executeSingle();
    }

    @Override
    public List<T> commitEntities(List<T> entities) {
        List<T> entitiesResponse = new ArrayList<>();
        for (T entity : entities) {
            entitiesResponse.add(commitEntity(entity));
        }
        return entitiesResponse;
    }

    @Override
    public T commitEntity(T entity) {
        if (!isEntityExist(entity)) {
            entity.save();
        } else {
            entity = updateEntity(entity);
        }
        return entity;
    }

    @Override
    public void removeById(long id) {
        new Delete().from(this.subModel).where("id=?", id).execute();
    }

    protected abstract boolean isEntityExist(T model);

    protected abstract T updateEntity(T entity);
}
