package com.hack.dpg.shoplocator.repository;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.DataController;
import com.hack.dpg.shoplocator.data.db.ShopDataBaseController;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.net.ShopNetworkController;
import com.hack.dpg.shoplocator.data.pojo.ShopDeleteResponse;
import com.hack.dpg.shoplocator.data.pojo.ShopUpdateResponse;
import com.hack.dpg.shoplocator.di.ShopModule;
import com.hack.dpg.shoplocator.network.EntityMonada;

import java.io.IOException;

import javax.inject.Inject;

public class ShopRepositoryImpl implements ShopRepository {

    @Inject
    public ShopControllerFactory factory;
    @Inject
    public ShopNetworkController networkController;

    public ShopRepositoryImpl() {
        ShoplocatorApplication.component()
                .plus(new ShopModule())
                .inject(this);
    }

    @Override
    public EntityMonada<Shop> findShops() {

        DataController<Shop> controller = this.factory.provideController();
        EntityMonada<Shop> entityMonada;
        try {
            entityMonada = new EntityMonada<>(controller.findAll(), null);
        } catch (Exception e) {
            entityMonada = new EntityMonada<>(null, e);
        }
        return entityMonada;
    }

    @Override
    public EntityMonada<Shop> findShopsByUserId(long userId) {
        EntityMonada<Shop> entityMonada;
        try {
            entityMonada = new EntityMonada<>(this.networkController.findShopsByUserId(userId), null);
        } catch (Exception e) {
            ShopDataBaseController dataBaseController = new ShopDataBaseController();
            String condition = Shop.FIELD_USER_ID + "=" + userId;
            entityMonada = new EntityMonada<>(dataBaseController.findByCondition(condition), e);
        }
        return entityMonada;
    }

    @Override
    public boolean delete(long shopId) {
        ShopDataBaseController dataBaseController = new ShopDataBaseController();
        long remoteId = dataBaseController.findEntityById(shopId).getShopId();
        try {
            ShopDeleteResponse response = this.networkController.removeShopById(remoteId);
            if (response != null && (response.getResponse().getCode() == 2000 || response.getResponse().getCode() == 2001)) {
                dataBaseController.removeById(shopId);
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public String save(Shop shop) {
        try {
            ShopUpdateResponse response = this.networkController.updateShop(shop);
            if (response.getResponse() != null
                    && response.getResponse().getCode() == 2000) {
                ShopDataBaseController dataBaseController = new ShopDataBaseController();
                Shop newShop = new Shop.Builder()
                        .setShopId(response.getResponse().getShopId())
                        .setDescription(shop.getDescription())
                        .setImage(shop.getImage())
                        .setLatitude(shop.getLatitude())
                        .setLongitude(shop.getLongitude())
                        .setTitle(shop.getTitle())
                        .setUserId(shop.getUserId())
                        .create();
                dataBaseController.commitEntity(newShop);
                return null;
            } else {
                return "Server error";
            }
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
    }
}

