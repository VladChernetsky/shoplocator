package com.hack.dpg.shoplocator.mvp.model.settings;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.utils.SharedPreferencesUtils;

import javax.inject.Inject;

public class TimeDialogModelImpl implements TimeDialogModel {

    public static final int TIME_DEFAULT = 1;
    public static final String PREFERENCE_TIME_TAG = "preference_time_tag";

    @Inject
    public SharedPreferencesUtils preferencesUtils;

    public TimeDialogModelImpl() {
        ShoplocatorApplication.component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @Override
    public String getTimeFromLocalStorage() {
        if (this.preferencesUtils.hasIntState(PREFERENCE_TIME_TAG))
            return String.valueOf(this.preferencesUtils.getIntState(PREFERENCE_TIME_TAG));
        return String.valueOf(TIME_DEFAULT);
    }

    @Override
    public void saveTimeToLocalStorage(int time) {
        this.preferencesUtils.setIntState(PREFERENCE_TIME_TAG, time);
    }
}
