package com.hack.dpg.shoplocator.mvp.view.shop;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.view.BaseView;
import com.hack.dpg.shoplocator.mvp.view.adapters.ShopsAdapter;

import java.util.List;

public interface ShopsView extends BaseView, ShopsAdapter.ShopClickListener {

    void displayShops(List<Shop> shopList);

    void displayShopForm(long shopId);

    void finishActionMode();

    void setActionModeTitle(String title);

    void toggleSelectedItem(int position);

    void showProgressBar();

    void hideProgressBar();
}
