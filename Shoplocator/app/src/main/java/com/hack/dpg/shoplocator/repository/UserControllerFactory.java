package com.hack.dpg.shoplocator.repository;

import com.hack.dpg.shoplocator.data.DataController;
import com.hack.dpg.shoplocator.data.entity.User;

public interface UserControllerFactory {
    DataController<User> provideUserController();
}
