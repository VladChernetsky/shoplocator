package com.hack.dpg.shoplocator.mvp.model.shop;

import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.mvp.presenter.shop.ShopsPresenterImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FakeShopsModelImpl implements ShopsModel {

    private final int COUNT_SHOPS = 15;
    private List<Shop> shops;

    private List<Shop> createShops() {
        shops = new ArrayList<>();
        for (int i = 0; i < COUNT_SHOPS; i++) {

            Shop shop = new Shop.Builder()
                    .setTitle("shop-name-" + i)
                    .setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
                            "nisi ut aliquip ex ea commodo consequat. Description-" + i)
                    .setUserId(i)
                    .setImage("image-" + i + ".png")
                    .create();
            shops.add(shop);
        }
        return shops;
    }

    @Override
    public void getShops(ShopsPresenterImpl.ShopCallback callback) {
        callback.onShopsLoaded(returnShops());
    }

    @Override
    public void deleteShops(List<Long> shopsIds, Runnable callback) {
        Iterator iterator = shops.iterator();
        while (iterator.hasNext()) {
            Shop shop = (Shop) iterator.next();
            if (shopsIds.indexOf(shop.getId()) > -1)
                iterator.remove();
        }
        callback.run();
    }

    private List<Shop> returnShops() {
        if (shops != null && shops.size() == 0)
            return shops;
        return createShops();
    }

    @Override
    public void handleCountSelectedItems(int count, ShopsPresenterImpl.SelectedItemCountListener itemCountListener) {
        if (count == 0) {
            itemCountListener.closeActionMode();
        } else {
            itemCountListener.getSelectedItemCount(count);
        }
    }
}
