package com.hack.dpg.shoplocator.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtilsImpl implements SharedPreferencesUtils {

    private static final String PREFERENCES = "SHOPLOCATOR";

    private SharedPreferences sharedPreferences;

    public SharedPreferencesUtilsImpl(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public void setIntState(String key, int value) {
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    @Override
    public int getIntState(String key) {
        return this.sharedPreferences.getInt(key, 0);
    }

    @Override
    public boolean hasIntState(String key) {
        return this.sharedPreferences.getInt(key, 0) != 0;
    }

    @Override
    public boolean clearAllStates() {
        return this.sharedPreferences.edit().clear().commit();
    }
}