package com.hack.dpg.shoplocator.di.components;

import com.hack.dpg.shoplocator.di.MapsModule;
import com.hack.dpg.shoplocator.mvp.model.maps.MapsModelImpl;
import com.hack.dpg.shoplocator.mvp.presenter.maps.MapsPresenterImpl;
import com.hack.dpg.shoplocator.mvp.view.maps.MapsActivity;

import dagger.Subcomponent;

@Subcomponent(modules = MapsModule.class)
public interface MapComponents {

    void inject(MapsActivity activity);

    void inject(MapsPresenterImpl presenter);

    void inject(MapsModelImpl model);
}
