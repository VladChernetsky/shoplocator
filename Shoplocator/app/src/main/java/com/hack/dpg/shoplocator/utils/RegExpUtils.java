package com.hack.dpg.shoplocator.utils;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpUtils {

    private static final String LATITUDE_PATTERN = "^(\\+|-)?(?:90(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,6})?))$";
    private static final String LONGITUDE_PATTERN = "^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))$";

    public static boolean isLongitude(String longitude) {
        Pattern pattern = Pattern.compile(LONGITUDE_PATTERN);
        Matcher matcher = pattern.matcher(longitude);
        return matcher.find();
    }

    public static boolean isLatitude(String longitude) {
        Pattern pattern = Pattern.compile(LATITUDE_PATTERN);
        Matcher matcher = pattern.matcher(longitude);
        return matcher.find();
    }

    public static boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }
}
