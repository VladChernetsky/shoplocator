package com.hack.dpg.shoplocator.repository;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.DataController;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.network.EntityMonada;

import javax.inject.Inject;

public class UserRepositoryImpl implements UserRepository {

    @Inject
    public UserControllerFactory factory;

    public UserRepositoryImpl() {
        ShoplocatorApplication.component()
                .plus(new UserModule())
                .inject(this);
    }

    @Override
    public EntityMonada<User> findUsers() {
        DataController<User> controller = this.factory.provideUserController();
        EntityMonada<User> entityMonada;
        try {
            entityMonada = new EntityMonada<>(controller.findAll(), null);
        } catch (Exception e) {
            entityMonada = new EntityMonada<>(null, e);
        }
        return entityMonada;
    }
}
