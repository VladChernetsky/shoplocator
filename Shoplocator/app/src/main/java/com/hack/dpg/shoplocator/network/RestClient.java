package com.hack.dpg.shoplocator.network;


import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static final String BASE_URL = "http://granitmach.ru";

    private static RestClient restClient;

    private RestService restService;

    private RestClient() {
        initService();
    }

    public static RestClient getInstance() {
        if (restClient == null)
            restClient = new RestClient();
        return restClient;
    }

    private void initService() {
        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .serializeNulls()
                        .create()))
                .build();

        this.restService = client.create(RestService.class);
    }

    public RestService restService() {
        return this.restService;
    }
}
