package com.hack.dpg.shoplocator.mvp.model.settings;

public interface TimeDialogModel {

    String getTimeFromLocalStorage();

    void saveTimeToLocalStorage(int time);
}
