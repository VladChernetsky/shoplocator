package com.hack.dpg.shoplocator.mvp.view.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hack.dpg.shoplocator.data.entity.Shop;

import java.util.List;

public interface MapsView {

    void initViewPager(List<Shop> shops);

    void displayMarkers(List<MarkerOptions> markerList);

    void moveCameraToMarker(LatLng latLng);

    void setViewPagerPositionByShop(int position);

    void initSearchView(String[] shops);

    void saveActiveShop(long shopId);
}
