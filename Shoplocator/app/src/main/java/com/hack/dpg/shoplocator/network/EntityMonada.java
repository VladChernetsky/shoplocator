package com.hack.dpg.shoplocator.network;

import java.util.List;

public class EntityMonada<T> {

    private List<T> entities;
    private Exception exception;

    public EntityMonada(List<T> entities, Exception exception) {
        this.entities = entities;
        this.exception = exception;
    }

    public List<T> getEntity() {
        return this.entities;
    }

    public Exception getException() {
        return this.exception;
    }

    public boolean hasError() {
        return this.exception != null;
    }
}
