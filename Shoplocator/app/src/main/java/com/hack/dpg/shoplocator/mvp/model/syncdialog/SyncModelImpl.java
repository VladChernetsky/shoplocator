package com.hack.dpg.shoplocator.mvp.model.syncdialog;

import android.os.AsyncTask;

import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.SettingsModule;
import com.hack.dpg.shoplocator.mvp.presenter.syncdialog.SyncPresenterImpl;
import com.hack.dpg.shoplocator.network.EntityMonada;
import com.hack.dpg.shoplocator.repository.ShopRepository;
import com.hack.dpg.shoplocator.repository.UserRepository;

import javax.inject.Inject;

public class SyncModelImpl implements SyncModel {

    @Inject
    public ShopRepository shopRepository;

    @Inject
    public UserRepository userRepository;

    private SyncPresenterImpl.SyncDataListener syncListener;

    public SyncModelImpl() {
        ShoplocatorApplication.component()
                .plus(new SettingsModule())
                .inject(this);
    }

    @Override
    public void setSyncListener(SyncPresenterImpl.SyncDataListener syncListener) {
        this.syncListener = syncListener;
    }

    @Override
    public void syncData() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                syncUsers(() -> syncShops());
                return null;
            }
        }.execute();
    }

    private void syncUsers(Runnable runnable) {
        EntityMonada<User> entityMonada = this.userRepository.findUsers();
        if (entityMonada.hasError()) {
            this.syncListener.onFailure(entityMonada.getException().getMessage());
        } else {
            runnable.run();
        }
    }

    private void syncShops() {
        EntityMonada<Shop> entityMonada = this.shopRepository.findShops();
        if (entityMonada.hasError()) {
            this.syncListener.onFailure(entityMonada.getException().getMessage());
        } else {
            this.syncListener.onSuccess();
        }
    }
}
