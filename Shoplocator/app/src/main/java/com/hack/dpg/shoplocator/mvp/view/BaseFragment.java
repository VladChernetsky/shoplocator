package com.hack.dpg.shoplocator.mvp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.hack.dpg.shoplocator.xml.CustomDialogView;
import com.hack.dpg.shoplocator.xml.CustomProgressDialog;

public abstract class BaseFragment extends Fragment implements BaseView {

    protected CustomDialogView dialogView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dialogView = new CustomProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.dialogView = null;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
