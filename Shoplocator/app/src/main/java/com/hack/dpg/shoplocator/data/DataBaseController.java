package com.hack.dpg.shoplocator.data;

import com.activeandroid.Model;

import java.util.List;

public interface DataBaseController<T extends Model> extends DataController<T> {
    
    List<T> findByCondition(String condition);

    T findEntityById(long id);

    List<T> commitEntities(List<T> entities);

    T commitEntity(T entity);

    void removeById(long id);
}
