package com.hack.dpg.shoplocator.mvp.presenter.user;

import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.mvp.presenter.BaseUpdatablePresenter;
import com.hack.dpg.shoplocator.mvp.view.user.UsersView;

public interface UsersPresenter extends BaseUpdatablePresenter {

    void onViewCreated(UsersView usersView);

    void onItemClicked(User user);
}
