package com.hack.dpg.shoplocator.mvp.model.user;

import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.mvp.presenter.user.UserDetailPresenterImpl;

public interface UserDetailModel {

    User getUserById(long userId);

    void getShopsByUserId(long userId, UserDetailPresenterImpl.ShopsCallback callback);
}
