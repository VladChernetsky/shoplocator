package com.hack.dpg.shoplocator.data.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "user")
public class User extends Model {

    public static final String FIELD_USER_ID = "userId";
    public static final String FIELD_USER_NAME = "username";
    public static final String FIELD_ADDRESS = "address";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_PICTURE = "picture";

    @Column(name = FIELD_USER_ID, unique = true, index = true)
    private long userId;

    @Column(name = FIELD_USER_NAME)
    private String name;

    @Column(name = FIELD_ADDRESS)
    private String address;

    @Column(name = FIELD_PHONE)
    private String phone;

    @Column(name = FIELD_PICTURE)
    private String picture;

    public User() {
        super();
    }

    public User(long userId, String name, String address, String phone, String picture) {
        super();
        this.userId = userId;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getPicture() {
        return picture;
    }

    public long getUserId() {
        return userId;
    }
}
