package com.hack.dpg.shoplocator.mvp.view.main;

public interface FragmentSubscriber {

    void subscribeFragment(Updatable updatable);

    void unSubscribeFragment(Updatable updatable);
}
