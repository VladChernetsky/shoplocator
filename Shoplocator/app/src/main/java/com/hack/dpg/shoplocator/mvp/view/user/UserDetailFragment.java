package com.hack.dpg.shoplocator.mvp.view.user;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hack.dpg.shoplocator.R;
import com.hack.dpg.shoplocator.ShoplocatorApplication;
import com.hack.dpg.shoplocator.data.entity.Shop;
import com.hack.dpg.shoplocator.data.entity.User;
import com.hack.dpg.shoplocator.di.UserModule;
import com.hack.dpg.shoplocator.mvp.presenter.user.UserDetailPresenter;
import com.hack.dpg.shoplocator.mvp.view.BaseUpdatableFragment;
import com.hack.dpg.shoplocator.mvp.view.FragmentDetailClickListener;
import com.hack.dpg.shoplocator.mvp.view.adapters.UserShopsAdapter;
import com.hack.dpg.shoplocator.mvp.view.main.MainActivity;
import com.hack.dpg.shoplocator.xml.UserDescriptionItemView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class UserDetailFragment extends BaseUpdatableFragment implements UserDetailView {

    private static final String USER_ID_TAG = "userId";

    @Inject
    public UserDetailPresenter presenter;

    private FragmentDetailClickListener fragmentDetailClickListener;
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvAddress;
    private ImageView ivUserPhoto;
    private UserShopsAdapter userShopsAdapter;

    public interface ShopDetailClickListener {
        void onShopDetailClick(long shopId);
    }

    public static Fragment getInstance(long userId) {
        Fragment fragment = new UserDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(USER_ID_TAG, userId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShoplocatorApplication
                .component()
                .plus(new UserModule())
                .inject(this);

        this.presenter.onUserIdReceived(getArguments().getLong(USER_ID_TAG, 0));
    }

    @Override
    protected void onUpdateFragment() {
        this.presenter.onUpdateEventStarted();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.fragmentDetailClickListener = (MainActivity) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_detail, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDescriptionViews(view);
        initRecyclerView(view);
        this.presenter.onViewCreated(this);
    }

    private void initDescriptionViews(View view) {
        UserDescriptionItemView userName = (UserDescriptionItemView) view.findViewById(R.id.item_user_name);
        UserDescriptionItemView userAddress = (UserDescriptionItemView) view.findViewById(R.id.item_user_address);
        UserDescriptionItemView userPhone = (UserDescriptionItemView) view.findViewById(R.id.item_user_phone);
        this.tvName = (TextView) userName.findViewById(R.id.tv_description);
        this.tvAddress = (TextView) userAddress.findViewById(R.id.tv_description);
        this.tvPhone = (TextView) userPhone.findViewById(R.id.tv_description);
        this.ivUserPhoto = (ImageView) view.findViewById(R.id.iv_user_photo);
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_shop_list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        gridLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        userShopsAdapter = new UserShopsAdapter(getActivity(), new ArrayList<>(), shopId -> presenter.clickShopDetail(shopId));
        recyclerView.setAdapter(userShopsAdapter);
    }

    @Override
    public void displayUser(User user) {
        this.tvName.setText(user.getName());
        this.tvAddress.setText(user.getAddress());
        this.tvPhone.setText(user.getPhone());
        Picasso.with(getActivity()).load(user.getPicture()).placeholder(getActivity().getResources().getDrawable(R.drawable.ic_panorama)).into(this.ivUserPhoto);
    }

    @Override
    public void displayShops(List<Shop> shopList) {
        this.userShopsAdapter.updateShops(shopList);
    }

    @Override
    public void displayShopDetail(long shopId) {
        this.fragmentDetailClickListener.clickShopDetail(shopId);
    }

    @Override
    public void showProgressBar() {
        this.dialogView.show(getActivity(), getFragmentManager());
    }

    @Override
    public void hideProgressBar() {
        this.dialogView.dismissDialog();
    }
}
