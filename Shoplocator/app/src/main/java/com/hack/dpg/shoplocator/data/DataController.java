package com.hack.dpg.shoplocator.data;

import com.activeandroid.Model;

import java.io.IOException;
import java.util.List;

public interface DataController<T extends Model> {

    List<T> findAll() throws IOException;
}
