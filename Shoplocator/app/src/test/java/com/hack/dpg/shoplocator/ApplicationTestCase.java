package com.hack.dpg.shoplocator;

import android.content.Context;

import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;


@Config(
        manifest = "app/src/test/AndroidManifest.xml",
        constants = BuildConfig.class,
        application = ApplicationStub.class,
        sdk = 21
)
@RunWith(RobolectricTestRunner.class)
public abstract class ApplicationTestCase {

    public static Context context() {
        return RuntimeEnvironment.application;
    }

}
