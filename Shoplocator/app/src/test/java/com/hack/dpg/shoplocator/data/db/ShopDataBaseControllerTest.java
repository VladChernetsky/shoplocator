package com.hack.dpg.shoplocator.data.db;

import com.activeandroid.ActiveAndroid;
import com.hack.dpg.shoplocator.ApplicationTestCase;
import com.hack.dpg.shoplocator.data.entity.Shop;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class ShopDataBaseControllerTest extends ApplicationTestCase {

    private static final int TESTABLE_SHOP_ID = 1;
    public static final int DELTA = 0;
    private ShopDataBaseController controller;

    @Before
    public void setUp() {
        ActiveAndroid.initialize(context());

        this.controller = new ShopDataBaseController();
        this.controller.commitEntity(getTestableObject());
    }

    @After
    public void tearDown() {
        ActiveAndroid.dispose();
    }

    @Test
    public void testIsEntityExist() {
        Shop shop1 = new Shop.Builder()
                .setTitle("title")
                .setUserId(1)
                .create();

        Shop shop2 = new Shop.Builder()
                .setTitle("empty string")
                .setUserId(1)
                .create();

        Assert.assertEquals(true, this.controller.isEntityExist(shop1));
        Assert.assertNotEquals(true, this.controller.isEntityExist(shop2));
    }

    @Test
    public void testUpdateEntity() {

        Shop object = new Shop.Builder()
                .setShopId(TESTABLE_SHOP_ID)
                .setTitle("changed title")
                .setDescription("changed description")
                .setImage("http://some-url.com/some-image.png")
                .setLatitude(52.0)
                .setLongitude(28.0)
                .setUserId(2)
                .create();

        Shop updatedObject = this.controller.updateEntity(object);
        Assert.assertEquals(updatedObject.getTitle(), object.getTitle());
        Assert.assertEquals(updatedObject.getDescription(), object.getDescription());
        Assert.assertEquals(updatedObject.getImage(), object.getImage());
        Assert.assertEquals(updatedObject.getLatitude(), object.getLatitude(), DELTA);
        Assert.assertEquals(updatedObject.getLongitude(), object.getLongitude(), DELTA);
        Assert.assertEquals(updatedObject.getUserId(), object.getUserId());
    }

    @Test
    public void testFindShopByName() {
        Shop object = this.controller.findShopByName("title");
        Assert.assertNotNull(object);
        Assert.assertEquals(object.getShopId(), TESTABLE_SHOP_ID);
    }

    @Test
    public void testRemoveById() throws IOException {
        List<Shop> shopList = this.controller.findAll();
        Assert.assertNotNull(shopList);
        Assert.assertNotEquals(shopList.size(), 0);
        this.controller.removeById(TESTABLE_SHOP_ID);
        shopList = this.controller.findAll();
        Assert.assertEquals(shopList.size(), 0);
    }

    private Shop getTestableObject() {
        return new Shop.Builder()
                .setShopId(TESTABLE_SHOP_ID)
                .setTitle("title")
                .setDescription("description")
                .setImage("http://some-url.com/some_image.jpg")
                .setLatitude(0.0)
                .setLongitude(0.0)
                .setUserId(1)
                .create();
    }
}
