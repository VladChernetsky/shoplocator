package com.hack.dpg.shoplocator.data.db;

import com.activeandroid.ActiveAndroid;
import com.hack.dpg.shoplocator.ApplicationTestCase;
import com.hack.dpg.shoplocator.data.DataBaseController;
import com.hack.dpg.shoplocator.data.entity.User;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class UserDataBaseControllerTest extends ApplicationTestCase {

    private DataBaseController<User> controller;

    @Before
    public void setUp() {
        ActiveAndroid.initialize(context());
        this.controller = new UserDataBaseController();
    }

    @After
    public void tearDown() {
        ActiveAndroid.dispose();
    }

    @Test
    public void testCommitEntity() throws IOException {
        int expectedCount = this.controller.findAll().size();
        User user = new User();
        this.controller.commitEntity(user);
        int actualCount = this.controller.findAll().size();
        Assert.assertEquals(expectedCount + 1, actualCount);
    }
}
